function generate_density_from_SPR(Material_file,SPR_file,Scanner_dir)

[HU_material,Material_Data] = MC2_import_scanner_file(Material_file);
[HU,SPR_Data] = MC2_import_scanner_file(SPR_file);

[~,~,SP,~,Water_SP] = HU_convert(HU,HU,ones(size(HU)),HU_material,Material_Data);

Density_Data = SPR_Data * Water_SP ./ SP;

output_filename = fullfile(Scanner_dir, 'HU_Density_Conversion.txt');

if(exist(output_filename,'file'))
   copyfile(output_filename,strrep(output_filename,'.txt','_bkp.txt'));
end

fid = fopen(output_filename,'w');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%s\n', '# HU	density g/cm3');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%d\t%1.6f\n', [HU;Density_Data]);
fclose(fid);