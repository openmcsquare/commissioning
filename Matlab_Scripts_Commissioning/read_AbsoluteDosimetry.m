function [Dose_meas,energy,isoc_depth,meas_depths,nbspots1,nbspots2,spot_spacing,nozzle_dist] = read_AbsoluteDosimetry(File, Calibration_depth, Iso_depth, field_size, spot_spacing, SAD_factor, IC_correction_factor, sadx, sady, app)

if(nargin<10)
    app = [];
end
if(nargin<9)
    sady = [];
end
if(nargin<8)
    sadx = [];
end
if(nargin<7)
    IC_correction_factor = [];
end
if(nargin<6)
    SAD_factor = [];
end
if(nargin<5)
    spot_spacing = [];
end
if(nargin<4)
    field_size = [];
end
if(nargin<3)
    Iso_depth = [];
end
if(nargin<2)
    Calibration_depth = [];
end

format = 'RayStation';
[~,~,fExt] = fileparts(File);

try
    mdata = textread(File,'%s','delimiter','\n');    
    %if contains(mdata{1},'RayStation') % RayStation format  
    if strcmp(fExt, '.csv')==1
        mask_before = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Measurement (Nominal')));
        mask_after = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'End:')));
        data = regexprep(mdata(mask_before(end)+1:mask_after(end)-1),'[^-+a-zA-Z0-9.]',' ');
        for l=1:size(data,1)
            Data(l,:) = str2double(strsplit(strtrim(data{l}),' '));
        end
    else % Eclipse format
        format = 'Eclipse';
        mask_before = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'<')));
        dmb = diff(mask_before)';
        ind = find(dmb>1);
        mask_before = [mask_before(1); mask_before(ind+1)];
        mask_after = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'>')));
        dmb = diff(mask_after)';
        mask_after = [mask_after(dmb>1); mask_after(end)];
        data = regexprep(mdata(mask_before(end)+1:mask_after(end)-1),'[^-+a-zA-Z0-9.]',' ');
        for l=1:size(data,1)
            Data(l,:) = str2double(strsplit(strtrim(data{l}),' '));
        end
    end
catch
    if(not(isempty(app)))
        uialert(app.UIFigure,'Error in parsing input file.', 'Error');
    else
        disp('Error in parsing input file. Abort absolute dosimetry data import.')
    end
    return
end

if(isempty(Data))
    if(not(isempty(app)))
        uialert(app.UIFigure,'No absolute dosimetry data found in this file. Check input file.', 'Error');
    else
        disp('No absolute dosimetry data found. Abort import.')
    end
    return
end


% Read RayStation data

if strcmp(format,'RayStation')
    
    energy = Data(:,1); % in MeV
    meas_depths = Data(:,2); % depths of measurement in mm
    mask_isodepth = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Isocenter to phantom surface distance')));
    if (mask_isodepth(1) < mask_before(1)) % check if iso is given once for all energies or for each energy
        isoc_depth = str2double(regexprep(mdata(mask_isodepth(1)),'[^0-9.]',' '))*ones(length(energy),1);
        Dose_meas = Data(:,3)/100; % Dose given in cGy/MU, now converted in Gy/MU
    else
        isoc_depth = Data(:,3);
        Dose_meas = Data(:,4)/100;
    end
    mask_spots = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Spot spacing')));
    spot_spacing = str2double(regexprep(mdata(mask_spots(1)),'[^0-9.]',' '));
    mask_fs = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Scanned field size')));
    field_size = str2double(strsplit(strtrim(string(regexprep(mdata(mask_fs(1)),'[^0-9.]',' '))),' '));
    if size(field_size,2)==2 % check if field size is given as mxn or just n
        nbspots1 = round(field_size(1)/spot_spacing + 1);
        nbspots2 = round(field_size(2)/spot_spacing + 1);
    else
        nbspots1 = round(field_size(1)/spot_spacing + 1);
        nbspots2 = nbspots1;
    end
    mask_nozzle_dist = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Isocenter to snout distance')));    
    nozzle_dist = str2double(regexprep(mdata(mask_nozzle_dist(1)),'[^0-9.]',''));
    
else % Eclipse
       
    mask_energy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'ENERGY')));
    for i=1:length(mask_energy)
        energy(i) = str2double(regexprep(mdata(mask_energy(i)),'[^0-9.]',''));
    end
    meas_depths = Calibration_depth.*ones(length(mask_energy),1);
    isoc_depth = Iso_depth(:).*ones(length(mask_energy),1);
    if size(field_size,2)==2 % check if field size is given as nxn or just n
        nbspots1 = round(field_size(1)/spot_spacing(1) + 1);
        nbspots2 = round(field_size(2)/spot_spacing(2) + 1);
    else
        nbspots1 = round(field_size(1)/spot_spacing(1) + 1);
        nbspots2 = nbspots1;
    end
    
    for j=1:length(mask_energy)
        % Measured data
        k = find(energy==energy(j));
        idd = regexprep(mdata(mask_before(k):mask_after(k)),'[<>]','');
        for l=1:size(idd,1)
            idd_tt(l,:) = str2double(strsplit(strtrim(idd{l}),' '));
        end
        tmpind = find(abs(idd_tt(1,1:3)-idd_tt(2,1:3))>1e-4);
        IDD = idd_tt(:, [tmpind 4]);
        spacingx = (sadx+meas_depths(j)-isoc_depth(j))/sadx*spot_spacing(1); % spot spacing at iso
        spacingy = (sady+meas_depths(j)-isoc_depth(j))/sady*spot_spacing(end); % spot spacing at iso
        MUfactor = 1/(spacingx*spacingy);
        Dose_meas(j,1) = interp1(IDD(:,1), IDD(:,2), meas_depths(j))*MUfactor/(nbspots1*nbspots2)/SAD_factor/IC_correction_factor;
        clearvars idd_tt idd IDD;
    end
    nozzle_dist = NaN;  
    
end
