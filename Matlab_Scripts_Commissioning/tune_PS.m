function [] = tune_PS(BDLname, nozzle2iso, MeasFile, flag_DG, p_bar)


global use_gui;

if(nargin<5)
    p_bar = [];
end


%% Data,...

% Read BDL
A = textread(BDLname,'%s','delimiter','\n');
mask_header = find(~cellfun(@isempty,strfind(A(1:length(A)-1),'NominalEnergy')));
BDL_parameters = importdata(BDLname, ' ', mask_header);
energy = BDL_parameters.data(:,1);

% Meas file
[mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,data_fmt] = read_SpotProfiles(MeasFile);
depths = unique(all_depths);
if(strcmp(data_fmt, 'RayStation')) % RayStation format
    xycurve = {'X', 'Y'};
else % Eclipse format
    xycurve = {'MeasuredSpotFluenceX', 'MeasuredSpotFluenceY'};
end


%% Fitting - tuning of PS for all commissioning energies

for j = 1:length(energy)
        
    if(not(isempty(p_bar)))
        commissioning_progress(p_bar,(j-1)/length(energy));
    end
    
    clearvars coeff;
    ind1 = find(round(all_energies*1000)/1000==energy(j));
    
    dsid = [];
    X_tot = [];
    Z_tot = [];
        
    for i = 1:2
        
        ind2 = find(all_curves==xycurve{i});
        
        for k = 1:length(depths)
            
            ind3 = find(all_depths==depths(k));
            ind = intersect(intersect(ind1,ind2),ind3);
            
            if(isempty(ind))
                continue
            end
            
            clearvars Z X info_image resolution x y X ind_max_x ind_max_y f data;

            % Data
            if(strcmp(data_fmt, 'RayStation')) % RayStation format
                z = regexprep(mdata(mask_begin(ind)+1:mask_end(ind)-1),'[;,]',' ');
                if(isempty(z))
                    disp('No data found');
                end
                for l=1:size(z,1)
                    data(l,:) = str2double(strsplit(strtrim(z{l}),' '));
                end
                if data(5,i) >= data(1,i)
                    X = data(:,i);
                    zz = data(:,3);
                else
                    X = flip(data(:,i));
                    zz = flip(data(:,3));
                end
            else
                z = regexprep(mdata(mask_begin(ind)+1:mask_end(ind)-1),'[<>]','');
                if(isempty(z))
                    disp('No data found');
                end
                for l=1:size(z,1)
                    data(l,:) = str2double(strsplit(strtrim(z{l}),' '));
                end
                indd = find(abs(data(1,1:3)-data(2,1:3))>1e-4);
                X = data(:,indd(1));
                zz = data(:,4);
            end

            % Normalize
            zz(zz<0) = 0;
            Z = zz./trapz(X,zz);
            
            % Data for fit
            dsid = [dsid; k*ones(size(X)) + (i-1)*length(depths)];
            X_tot = [X_tot; X];
            Z_tot = [Z_tot; Z];           
            
        end
        
    end
        
    % Parameters and function for Gaussian fits

    if flag_DG == 1

        f0 = @(M, S, W, s, X_tot) (W./sqrt(2*pi*S(dsid).^2).*exp((-1*(X_tot-M(dsid)).^2)./(2*S(dsid).^2)) + (1-W)./sqrt(2*pi*s(dsid).^2).*exp((-1*(X_tot-M(dsid)).^2)./(2*s(dsid).^2)));
        f1 = @(params,X_tot) f0(params(1:length(depths)*2),params(length(depths)*2+1:length(depths)*4),params(length(depths)*4+1),params(length(depths)*4+2:length(depths)*6+1),X_tot);
        f2 = @(M, S, W, s, X_tot) (W./sqrt(2*pi*S.^2).*exp((-1*(X_tot-M).^2)./(2*S.^2)) + (1-W)./sqrt(2*pi*s.^2).*exp((-1*(X_tot-M).^2)./(2*s.^2))); 
        startpts = [zeros(2*length(depths),1); 6*ones(2*length(depths),1); 1; 18*ones(2*length(depths),1)];
        lb = [-1000*ones(2*length(depths),1); 0.1*ones(2*length(depths),1); 0.5; 0.1*ones(2*length(depths),1)];
        ub = [1000*ones(2*length(depths),1); 20*ones(2*length(depths),1); 1; Inf*ones(2*length(depths),1)];

    else

        f0 = @(M, S, W, X_tot) (W./sqrt(2*pi*S(dsid).^2).*exp((-1*(X_tot-M(dsid)).^2)./(2*S(dsid).^2)));
        f1 = @(params,X_tot) f0(params(1:2*length(depths)),params(length(depths)*2+1:length(depths)*4),params(length(depths)*4+1),X_tot);
        f2 = @(M, S, W, X_tot) (W./sqrt(2*pi*S.^2).*exp((-1*(X_tot-M).^2)./(2*S.^2)));
        startpts = [zeros(2*length(depths),1); 6*ones(2*length(depths),1); 1];
        lb = [-1000*ones(2*length(depths),1); 0.1*ones(2*length(depths),1); 0.5];
        ub = [1000*ones(2*length(depths),1); 20*ones(2*length(depths),1); 1];

    end


    % Fit
    opts = optimset('display','off');
    sol = lsqcurvefit(f1,startpts,X_tot,Z_tot,lb,ub,opts);

    % Auto check - Gaussian fits
    for jj=1:length(depths)
        for ii=1:2
            if (flag_DG==0)
                Zfit = f2( sol(jj+(ii-1)*length(depths)), sol(jj+2*length(depths)+(ii-1)*length(depths)), sol(length(depths)*4+1), X_tot);
            else
                Zfit = f2(sol(jj+(ii-1)*length(depths)), sol(jj+2*length(depths)+(ii-1)*length(depths)), sol(length(depths)*4+1), sol(jj + length(depths)*4+1+(ii-1)*length(depths)), X_tot);
            end
            Zdiff = abs(Z_tot(dsid == jj+(ii-1)*length(depths))-Zfit(dsid == jj+(ii-1)*length(depths)));
            Zerr = max(Zdiff)/max(Z_tot(dsid == jj+(ii-1)*length(depths))); % max relative error   
            if (Zerr>0.02)
                disp('WARNING: Gaussian fit might not be accurate enough.');
                if(use_gui)
                    % Plot
                    figure
                    color = rand(1,3)*0.75;
                    scatter(X_tot(dsid == jj+(ii-1)*length(depths)), Z_tot(dsid == jj+(ii-1)*length(depths)), [], color);
                    if (flag_DG==0)
                        hh = line(X_tot(dsid == jj+(ii-1)*length(depths)), f2( sol(jj+(ii-1)*length(depths)), sol(jj+2*length(depths)+(ii-1)*length(depths)), sol(length(depths)*4+1), X_tot(dsid == jj+(ii-1)*length(depths)))); %, sol(jj + length(depths)*4+1)
                        hh.Color = color;
                    else
                        hh = line(X_tot(dsid == jj+(ii-1)*length(depths)), f2(sol(jj+(ii-1)*length(depths)), sol(jj+2*length(depths)+(ii-1)*length(depths)), sol(length(depths)*4+1), sol(jj + length(depths)*4+1+(ii-1)*length(depths)), X_tot(dsid == jj+(ii-1)*length(depths)))); %, sol(jj + length(depths)*4+1)
                        hh.Color = color;
                    end
                    %set(gca,'yscale','log')
                    title(['WARNING: abnormal spot size fit (', xycurve{ii}, ', ', num2str(energy(j)),' MeV, ',num2str(depths(jj)),' mm depth)']);
                    legend('Measured data', 'Fit');
                    drawnow;
                end
            end
             clearvars Zdiff Zerr;
        end
    end   
  
    % Courant Snyder fits
    coeff(:,1,1) = sol(1:length(depths));
    coeff(:,1,2) = sol(length(depths)+1:2*length(depths));
    
    for i=1:2
        
        for l=1:1+flag_DG % 1 if single gauss or 2 if double gauss
        
            % Getting std of gaussian previously found
            coeff(:,l+1,i) = sol(length(depths)*2+1 +length(depths)*(i-1) +(length(depths)*2+1)*(l-1):3*length(depths) +length(depths)*(i-1) +(length(depths)*2+1)*(l-1));

            % Initial value for divergence
            if (l==1)
                div = polyfit(depths', coeff(:,2,i), 1);
            end

            if (div(1)>0) % convergent beam
                div_flag(j,:,i) = (abs(sort(coeff(:,l+1,i), 'ascend')-coeff(:,l+1,i)) > 1e-4);
                X0 = [interp1(depths, coeff(:,l+1,i), 0), abs(div(1)), -0.6];
                lb = [0,0,-0.99];
                ub = [20,0.5,0];
            else % divergent beam
                div_flag(j,:,i) = (abs(sort(coeff(:,l+1,i), 'descend')-coeff(:,l+1,i)) > 1e-4);
                X0 = [interp1(depths, coeff(:,l+1,i), 0), abs(div(1)), -0.6];
                lb = [0,0,0];
                ub = [20,0.5,0.99];
            end
            
            % Courant-Snyder            
            func = @(x,xdata) sqrt(x(1)^2 - 2*x(3)*x(1)*x(2)*xdata + x(2)^2*xdata.^2);
            opts = optimset('display','off');
            [coeff_cs(j,[1 2 3] + 3*(l-1),i), resnorm] = lsqcurvefit(func, X0, depths', coeff(:,l+1,i), lb, ub, opts);
            rmse = sqrt(resnorm/length(depths));
            adjr2 = 1 - (rmse)^2/var(coeff(:,l+1,i));
            gof_flag(j,1,i) = (adjr2<0.87);
            
            % Auto check
            if(gof_flag(j,1,i)>0)
                disp('WARNING: Courant-Snyder fit might not be accurate enough.');
                if(use_gui)
                    figure
                    plot(depths', coeff(:,2,i),'.');
                    hold on;
                    plot(depths', sqrt(coeff_cs(j,1,i)^2 - 2*coeff_cs(j,3,i)*coeff_cs(j,1,i)*coeff_cs(j,2,i)*depths' + coeff_cs(j,2,i)^2*(depths').^2));
                    title(['Courant-Snyder (',num2str(energy(j)),' MeV)'])
                    legend('Data','Fit')
                    drawnow
                end
            end
            
            % Computing parameters at nozzle exit thanks to CS
            s_nozzle(j,l,i) = sqrt(coeff_cs(j,1 + 3*(l-1),i)^2 - 2*coeff_cs(j,3 + 3*(l-1),i)*coeff_cs(j,1 + 3*(l-1),i)*coeff_cs(j,2 + 3*(l-1),i)*(nozzle2iso) + coeff_cs(j,2 + 3*(l-1),i)^2*nozzle2iso^2);
            corr_nozzle(j,l,i) = (coeff_cs(j,3 + 3*(l-1),i)*coeff_cs(j,1 + 3*(l-1),i) - coeff_cs(j,2 + 3*(l-1),i)*(nozzle2iso))/s_nozzle(j,l,i);
            
            % Writing to BDL
            v2 = [6 7 8] + 3*(i-1) + 7*(l-1);
            WriteToBDL(BDLname, energy(j), [5 12 v2], [sol(length(depths)*4+1)-(1-sol(length(depths)*4+1))*(flag_DG-1), flag_DG*(1-sol(length(depths)*4+1)), s_nozzle(j,l,i), coeff_cs(j,2 + 3*(l-1),i), corr_nozzle(j,l,i)]);
            
        end
        
    end
    
end
        
% In case of double gaussian, checking if it is really needed (results might be meaningless)
if ((sum(gof_flag(:)) + sum(div_flag(:)) > 0) && flag_DG==1)
    disp('WARNING: No double Gaussian needed: switching to single Gaussian');
    pause(2);
    WriteToBDL(BDLname, energy, 13:18, repmat([5 0.003 0.2 5 0.003 0.2],length(energy),1));
    tune_PS(BDLname, nozzle2iso, MeasFile, 0);
end

if(not(isempty(p_bar)))
    commissioning_progress(p_bar,1);
end
