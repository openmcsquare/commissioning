function [mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,format] = read_SpotProfiles(File,app)

if(nargin<3)
    app = [];
end

try
    % load file(s)
    if(iscell(File))
        [~,~,fExt] = fileparts(File{1});
        mdata = {};
        for i=1:numel(File)
            mdata = [mdata; textread(File{i},'%s','delimiter','\n')];
        end
    else
        [~,~,fExt] = fileparts(File);
        mdata = textread(File,'%s','delimiter','\n'); 
    end  
    
    % parse loaded data
    %if contains(mdata{1},'RayStation') % RayStation format
    if strcmp(fExt, '.csv')==1
        format = 'RayStation';
        mask_begin = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Measured fluence curve')));
        mask_end = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'End:')));
        mask_energy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Nominal beam energy')));
        mask_depth = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Air profile')));
        mask_xy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Curve type')));
        mask_nozzle_dist = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Isocenter to snout distance')));
        if(length(mask_end)<length(mask_begin))
            mask_end(end+1) = length(mdata); % missing 'End' at the end of the file
        end
    else % Eclipse format
        format = 'Eclipse';
        mask_begin = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'<')));
        dmb = diff(mask_begin)';
        ind = find(dmb>1);
        mask_begin = [mask_begin(1); mask_begin(ind+1)];
        mask_end = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'>')));
        dmb = diff(mask_end)';
        mask_end = [mask_end(dmb>1); mask_end(end)];
        mask_energy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'ENERGY')));
        mask_depth = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'ZPOS')));
        mask_xy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'TYPE')));
    end    
        
catch
    if(not(isempty(app)))
        uialert(app.UIFigure,'Error in parsing input file.', 'Error');
    else
        disp('Error in parsing input file. Abort spot profile import.')
    end
    return
end

if(isempty(mask_depth))
    if(not(isempty(app)))
        uialert(app.UIFigure,'No spot profile data found in this file. Check input file.', 'Error');
    else
        disp('No spot profile data found. Abort spot profile import.')
    end
    return
elseif(length(mask_depth)~=length(mask_xy) || length(mask_depth)~=length(mask_energy))
    if(not(isempty(app)))
        uialert(app.UIFigure,'Incorrect amount of data. Check input file.', 'Error');
    else
        disp('Incorrect amount of data. Abort spot profile import.')
    end
    return
end


if strcmp(format,'RayStation') % RayStation format
    
    for i=1:length(mask_depth)
        all_depths(i) = str2double(regexprep(mdata(mask_depth(i)),'[^-0-9.]',''));
        all_energies(i) = str2double(regexprep(mdata(mask_energy(i)),'[^0-9.]',''));
        a = strsplit(strtrim(string(mdata(mask_xy(i)))));
        all_curves(i) = regexprep(strtrim(a(end)),'[^A-Z]',''); %a(end)
        nozzle_dist(i) = str2double(regexprep(mdata(mask_nozzle_dist(i)),'[^0-9.]',''));       
    end
    
else
    
    for i=1:length(mask_depth)
        all_depths(i) = str2double(regexprep(mdata(mask_depth(i)),'[^-0-9.]',''));
        all_energies(i) = str2double(regexprep(mdata(mask_energy(i)),'[^0-9.]',''));
        a = strsplit(string(mdata(mask_xy(i))));
        all_curves(i) = a(end);
    end   
    nozzle_dist = NaN;
    
end
