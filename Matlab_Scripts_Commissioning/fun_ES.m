function [y] = fun_ES(x, IDD, WorkDir, new_spacing, new_size, origin, true_centre, IC_diameter, energy, BDL, MCsquareDir)

    global SimulatedProtons;

    x

    % Change value of mean energy and spread in BDL
    WriteToBDL(BDL, energy, [2 3], x);
    
    % MC simulation
    openreggui_Generate_MC2_Config(WorkDir, SimulatedProtons, 'CT.mhd', 'OneSpot.txt', fullfile(MCsquareDir, 'Scanners/Water_only'), BDL);
    disp(' ');
    disp('Start MCsquare simulation');
    if(ispc)
        copyfile(fullfile(MCsquareDir, 'Materials'), fullfile(WorkDir, 'Materials'));
        cd(WorkDir)
        eval(['!',fullfile(MCsquareDir, 'MCsquare.bat >> log.txt 2>>&1')]);
    else
        system(['cd ' WorkDir ' && ' fullfile(MCsquareDir, 'MCsquare_double')]);
    end
    [~, Dose_data] = openreggui_Import_MHD_Data(fullfile(WorkDir, 'Outputs/Dose.mhd'));
    
    % Vectors, grids
    v1 = origin(1):new_spacing(1):(origin(1) + (new_size(1)-1)*new_spacing(1));
    v2 = origin(2):new_spacing(2):(origin(2) + (new_size(2)-1)*new_spacing(2));
    v3 = origin(3):new_spacing(3):(origin(3) + (new_size(3)-1)*new_spacing(3));
    v1 = v1 + new_spacing(1)/2;
    v2 = v2 + new_spacing(2)/2;
    v3 = v3 + new_spacing(3)/2;
    [X,Y,Z] = meshgrid(v1, v2, v3);

    % Computation of MC depth dose
    Dose_data(sqrt((X-true_centre(1)).^2 + (Z-true_centre(3)).^2) > IC_diameter/2) = 0;
    Dosesum = squeeze(sum(sum(Dose_data, 2),3));
    absci = 0:length(Dosesum)-1;
    absci = absci.*new_spacing(2) + new_spacing(2)/2;
    MCdose = interp1(absci, Dosesum, IDD(:,1), 'linear', 'extrap');
    MCdose(MCdose<0) = 0;
    MCdose = MCdose./trapz(IDD(:,1), MCdose);
   
    % r80 relative error
    ind_before = find(MCdose>=max(MCdose)*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_simu = interp1([MCdose(ind_after); MCdose(ind_before)], [IDD(ind_after,1); IDD(ind_before,1)], max(MCdose)*80/100);
    ind_before = find(IDD(:,2)>=max(IDD(:,2))*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_meas = interp1([IDD(ind_after,2); IDD(ind_before,2)], [IDD(ind_after,1); IDD(ind_before,1)], max(IDD(:,2))*80/100);
    errel = (r80_meas-r80_simu);
    
    % r20 relative error
    ind_before = find(MCdose>=max(MCdose)*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_simu = interp1([MCdose(ind_after); MCdose(ind_before)], [IDD(ind_after,1); IDD(ind_before,1)], max(MCdose)*20/100);
    ind_before = find(IDD(:,2)>=max(IDD(:,2))*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_meas = interp1([IDD(ind_after,2); IDD(ind_before,2)], [IDD(ind_after,1); IDD(ind_before,1)], max(IDD(:,2))*20/100);
    errel20 = (r20_meas-r20_simu);
    
    % Dose to peak difference (%)
    dtpd = (max(IDD(:,2)) - max(MCdose))*100/max(IDD(:,2));
    
    % Mean pt-to-pt difference (%)
    L = r80_meas - IDD(1,1);
    mptpd = 0;
    for j=1:length(IDD(:,2))-1
       if (IDD(j,2) > max(IDD(:,2))/40)
          mptpd = mptpd + (abs(IDD(j,2) - MCdose(j))/IDD(j,2))*((IDD(j+1,1) - IDD(j,1))/L)*100; 
       end
    end
    
    % FWHM but at t%
    t = 60;
    ind_before_r = find(IDD(:,2)>=max(IDD(:,2))*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(IDD(:,2)>=max(IDD(:,2))*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_m = interp1([IDD(ind_after_r,2);IDD(ind_before_r,2)], [IDD(ind_after_r,1);IDD(ind_before_r,1)], max(IDD(:,2))*t/100);
    l_m = interp1([IDD(ind_before_l,2);IDD(ind_after_l,2)], [IDD(ind_before_l,1);IDD(ind_after_l,1)], max(IDD(:,2))*t/100);
    FWHM_meas = r_m-l_m;
    ind_before_r = find(MCdose>=max(MCdose)*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(MCdose>=max(MCdose)*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_s = interp1([MCdose(ind_after_r);MCdose(ind_before_r)], [IDD(ind_after_r,1);IDD(ind_before_r,1)], max(MCdose)*t/100);
    l_s = interp1([MCdose(ind_before_l);MCdose(ind_after_l)], [IDD(ind_before_l,1);IDD(ind_after_l,1)], max(MCdose)*t/100);
    FWHM_MC = r_s-l_s;
    err_FWHM = (FWHM_meas - FWHM_MC);
    
    clearvars ind_max ind_before ind_after ind_before2 ind_after2;
      
    mptpd
    errel
    dtpd
    err_FWHM
    
    % Objective function
    y = double(10*(errel^2) + errel20^2 + (1.2*err_FWHM-dtpd)^2)
   
    
end



