function [stop, options, optchanged] = StopCriterion(optimvalues, options, flag)

    global SimulatedProtons;

    stop = false;
    optchanged = false;
   

	if (optimvalues.meshsize < 0.02)
		SimulatedProtons = 3e7;
	end

	if (optimvalues.meshsize <= 0.005)
		SimulatedProtons = 4e7;
	end

	if (optimvalues.meshsize <= 0.0005)
		SimulatedProtons = 5e7;
    end



end
