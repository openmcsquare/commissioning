function [] = tune_AD(MCsquareDir, BDL, CT, MeasFile, new_spacing, detector, speed_mode, sadx, sady, RBE, Calibration_depth, Iso_depth, Field_size, Spots_spacing, SAD_factor, IC_correction_factor, nozzle_exit, p_bar)

global use_gui;
global use_debug;

if(nargin<18)
    p_bar = [];
end

if(use_debug)
    SimulatedProtons = 1e7;
else
    SimulatedProtons = 2e9 + speed_mode*(7e7 - 2e9);
end

WorkDir = fullfile(MCsquareDir, '..', 'WorkMC');
if(~exist(WorkDir))
    mkdir(WorkDir);
end
s1 = what(WorkDir);
p1 = s1.path;
clearvars s1;

%% Measurements data

% Measurements
[Dose_meas,energy,isoc_depth,meas_depths,nbspots1,nbspots2,spot_spacing] = read_AbsoluteDosimetry(MeasFile, Calibration_depth, Iso_depth, Field_size, Spots_spacing, SAD_factor, IC_correction_factor, sadx, sady, nozzle_exit);
nb_spots = nbspots1*nbspots2;
if (nb_spots==1 && use_debug==0)
    SimulatedProtons = 7e7;
    speed_mode = 0;
end

%% Computation of protons per MU

% Resampling CT (modifying only MHD file because here only water)
[filepath,name,ext] = fileparts(CT);
s2 = what(filepath);
p2 = s2.path;
clearvars s2;
if ext=='.dcm'
    [CT_info, ~] = openreggui_DicomCT_to_MHD(CT, fullfile(WorkDir, 'CT.mhd'));
elseif ext=='.mhd'
    A = textread(CT,'%s','delimiter','\n');
    mask_size = find(~cellfun(@isempty,strfind(A(1:length(A)),'DimSize')));
    mask_sp = find(~cellfun(@isempty,strfind(A(1:length(A)),'ElementSpacing')));
    mask_ori = find(~cellfun(@isempty,strfind(A(1:length(A)),'Offset')));
    mask_raw = find(~cellfun(@isempty,strfind(A(1:length(A)),'ElementDataFile')));
    CT_info.Size = str2double(strsplit(strtrim(string(regexprep(A(mask_size(1)),'[^0-9.]',' '))),' '));
    CT_info.Spacing = str2double(strsplit(strtrim(string(regexprep(A(mask_sp(1)),'[^0-9.]',' '))),' '));
    CT_info.ImagePositionPatient = str2double(strsplit(strtrim(string(regexprep(A(mask_ori(1)),'[^-0-9.]',' '))),' '));
    CTraw = strtrim(strsplit(string(A(mask_raw)),'='));
    if strcmp(string(p1),string(p2))==0
        copyfile(CT, WorkDir);
        copyfile(fullfile(p2, char(CTraw(2))), WorkDir);
    end
else
    error('Error: please enter a valid CT name (dcm or mhd)');
end
resolution_fact = CT_info.Spacing(:)./new_spacing(:);
mhdCT = fopen(fullfile(WorkDir, 'CT.mhd'), 'r');
i = 1;
tline = fgetl(mhdCT);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(mhdCT);
    A{i} = tline;
end
new_size = [round(CT_info.Size(1)*resolution_fact(1)), round(CT_info.Size(2)*resolution_fact(2)), round(CT_info.Size(3)*resolution_fact(3))];
A{3} = sprintf('DimSize = %d %d %d', new_size(1), new_size(2), new_size(3));
A{4} = sprintf('ElementSpacing = %f %f %f', new_spacing(1), new_spacing(2), new_spacing(3));
fclose(mhdCT);

% Rewrite mhd CT
f=fopen(fullfile(WorkDir, 'CT.mhd'), 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(f,'%s', A{i});
        break;
    else
        fprintf(f,'%s\n', A{i});
    end
end
fclose(f);
clearvars A;

% Computation of the CT centre
origin = CT_info.ImagePositionPatient;
true_centre = origin(:) + new_spacing(:)/2 + (new_size(:)-1).*new_spacing(:)/2;

% Correction for the origin (needed this way in the MC2 plan)
centre = true_centre - origin(:);

% Coordinates vectors
v1 = origin(1):new_spacing(1):(origin(1) + (new_size(1)-1)*new_spacing(1));
v2 = origin(2):new_spacing(2):(origin(2) + (new_size(2)-1)*new_spacing(2));
v3 = origin(3):new_spacing(3):(origin(3) + (new_size(3)-1)*new_spacing(3));
v1 = v1 + new_spacing(1)/2;
v2 = v2 + new_spacing(2)/2;
v3 = v3 + new_spacing(3)/2;

% Grids of coordinates (perpendicular plane to the beam)
[Z1,Y1] = meshgrid(v3, v1);

% List of spots
c1 = -(nbspots1-1)*spot_spacing/2 : spot_spacing : (nbspots1-1)*spot_spacing/2;
c2 = -(nbspots2-1)*spot_spacing/2 : spot_spacing : (nbspots2-1)*spot_spacing/2;
[C1, C2] = meshgrid(c1,c2);
C = cat(2, C1',C2');
coord = reshape(C,[],2);
coord(:,3) = ones(size(coord,1), 1);

% Read BDL
A = textread(BDL,'%s','delimiter','\n');
mask_header = find(~cellfun(@isempty,strfind(A(1:length(A)),'NominalEnergy')));
BDL_parameters = importdata(BDL, ' ', mask_header);
energybdl = BDL_parameters.data(:,1);

% Write the plan an compute protons/MU for each energy
for num_energ = 1:size(BDL_parameters.data,1)
    
    if(not(isempty(p_bar)))
        commissioning_progress(p_bar,(num_energ-1)/length(energybdl));
    end
    
    k = find(round(energy*1000)/1000==energybdl(num_energ),1);
    
    clearvars d ind Dose_data Dose_info;
    centre(2) = (new_size(2)-1)*new_spacing(2) - isoc_depth(k) + new_spacing(2)/2; % correction for isocenter depth
    
    % Write plan for MC
    fid = fopen(fullfile(WorkDir, 'plan_AD.txt'), 'w');
    if (speed_mode==0)
        fprintf(fid, '#TREATMENT-PLAN-DESCRIPTION\n#PlanName\nplan_AD\n#NumberOfFractions\n1\n##FractionID\n1\n##NumberOfFields\n1\n###FieldsID\n1\n#TotalMetersetWeightOfAllFields\n%d\n\n#FIELD-DESCRIPTION\n###FieldID\n1\n###FinalCumulativeMeterSetWeight\n%d\n###GantryAngle\n0\n###PatientSupportAngle\n0\n###IsocenterPosition\n%f\t%f\t%f\n###NumberOfControlPoints\n1\n\n#SPOTS-DESCRIPTION\n####ControlPointIndex\n1\n####SpotTunnedID\n1\n####CumulativeMetersetWeight\n%d\n####Energy (MeV)\n%f\n####NbOfScannedSpots\n%d\n####X Y Weight\n', nb_spots, nb_spots, centre(1), centre(2), centre(3), nb_spots, energybdl(num_energ), nb_spots);
        fprintf(fid, '%f %f %f\n', coord');
    else
        fprintf(fid, '#TREATMENT-PLAN-DESCRIPTION\n#PlanName\nOneSpot\n#NumberOfFractions\n1\n##FractionID\n1\n##NumberOfFields\n1\n###FieldsID\n1\n#TotalMetersetWeightOfAllFields\n1\n\n#FIELD-DESCRIPTION\n###FieldID\n1\n###FinalCumulativeMeterSetWeight\n1\n###GantryAngle\n0\n###PatientSupportAngle\n0\n###IsocenterPosition\n%f\t%f\t%f\n###NumberOfControlPoints\n1\n\n#SPOTS-DESCRIPTION\n####ControlPointIndex\n1\n####SpotTunnedID\n1\n####CumulativeMetersetWeight\n1\n####Energy (MeV)\n%f\n####NbOfScannedSpots\n1\n####X Y Weight\n0 0 1', centre(1), centre(2), centre(3), energybdl(num_energ));
    end
    fclose(fid);
    
    % MC simulation
    openreggui_Generate_MC2_Config(WorkDir, SimulatedProtons, 'CT.mhd', 'plan_AD.txt', fullfile(MCsquareDir, 'Scanners/Water_only'), BDL);
    disp(' ');
    disp('Start MCsquare simulation');
    if(ispc) % for Windows
        copyfile(fullfile(MCsquareDir, 'Materials'), fullfile(WorkDir, 'Materials'));
        cd(WorkDir)
        eval(['!',fullfile(MCsquareDir, 'MCsquare.bat >> log.txt 2>>&1')]);
    else
        system(['dos2unix ', fullfile(MCsquareDir, 'MCsquare_double')]);
        system(['cd ' WorkDir ' && ' fullfile(MCsquareDir, 'MCsquare_double')]);
    end
    [~, Dose_data] = openreggui_Import_MHD_Data(fullfile(WorkDir, 'Outputs/Dose.mhd'));
    Dose_data = Dose_data * 1.602176e-19 * 1000; % Dose/proton
    
    % For speed mode : rotate beamlet
    if (speed_mode==1)
                
        % Reduce dose by nb spots and remove useless part of Dose_data (below measurement depth + a tolerance)
        Dose_data = Dose_data/nb_spots;
        if (size(Dose_data,1) > meas_depths(k)/new_spacing(2) + 15/new_spacing(2))
            Dose_data = Dose_data(1:ceil(meas_depths(k)/new_spacing(2)+15/new_spacing(2)),:,:);
        end

        % Add zeros on top of Dose_data so that beamlet starts in the middle of the dose map
        sizeIn = size(Dose_data);
        
        % Initialize total field dose
        D = zeros(sizeIn);

        for i=1:nb_spots
                        
            alpha = rad2deg(-atan(coord(i,1)/sadx));
            beta = rad2deg(atan(coord(i,2)/sady));
            
            % Affine matrix 1
            shift = [(origin(1)-true_centre(1)) (sady-isoc_depth(k)) 0]';
            u = [0 0 1]';
            M1 = eye(4);
            M1(end,1:3) = -shift;
            M2 = eye(4);
            for i=1:3
                v = M2(1:3,i);
                R(:,i)=v*cosd(beta) + cross(u,v)*sind(beta) + (u.'*v)*(1-cosd(beta))*u;
            end
            M2(1:3,1:3) = R;
            Mtot1 = inv(M1)*M2*M1;
            
            % Affine matrix 2
            shift = [0 sadx-isoc_depth(k) (origin(3)-true_centre(3))]';
            u = [1 0 0]';
            M1 = eye(4);
            M1(end,1:3) = -shift;
            M2 = eye(4);
            for i=1:3
                v = M2(1:3,i);
                R(:,i)=v*cosd(alpha) + cross(u,v)*sind(alpha) + (u.'*v)*(1-cosd(alpha))*u;
            end
            M2(1:3,1:3) = R;
            Mtot2 = inv(M1)*M2*M1;
            MM = Mtot1*Mtot2;
            MM(:,end)=[0;0;0;1]; % just to avoid potential numerical issues

            % Rotate beamlet
            tform = affine3d(MM);
            sameAsInput = imref3d(sizeIn, new_spacing(1), new_spacing(2), new_spacing(3));
            rotated = imwarp(Dose_data, sameAsInput, tform, 'OutputView', sameAsInput);
          
            % Total dose
            D = D + rotated;
                        
        end
        
        % Total dose of all rotated spots
        Dose_data = D;
        
    end
    
    % Mean MC dose in IC volume
    x = new_spacing(2)/2:new_spacing(2):meas_depths(k)+1;
    ind = find(abs(x-meas_depths(k))==min(abs(x-meas_depths(k))));
    d = squeeze(mean(Dose_data(ind,:,:),1)); % dose matrix at a defined depths in plateau
    switch detector.Shape
        case 'square'
            if (nb_spots > 1)
                Dose_MC(num_energ) = mean(d( (abs(Y1-true_centre(1)))<=detector.SensitiveVolumeSize/2 & abs((Z1-true_centre(3)))<=detector.SensitiveVolumeSize/2 ));
            else
                d((abs(Y1-true_centre(1)))>detector.SensitiveVolumeSize/2 & abs((Z1-true_centre(3)))>detector.SensitiveVolumeSize/2) = 0;
                Dose_MC(num_energ) = trapz(v1,trapz(v3,d,2));
            end
        otherwise
            if (nb_spots > 1)
                Dose_MC(num_energ) = mean(d(sqrt((Y1-true_centre(1)).^2 + (Z1-true_centre(3)).^2) <= detector.SensitiveVolumeSize/2));
            else
                d(sqrt((Y1-true_centre(1)).^2 + (Z1-true_centre(3)).^2) > detector.SensitiveVolumeSize/2) = 0;
                Dose_MC(num_energ) = trapz(v1,trapz(v3,d,2));
            end
    end
    
    % Protons per MU
    protons_per_mu(num_energ,1) = RBE*Dose_meas(k)/Dose_MC(num_energ)
    
    % Write to BDL
    WriteToBDL(BDL, energybdl(num_energ), 4, protons_per_mu(num_energ,1));    
    
end

if(not(isempty(p_bar)))
    commissioning_progress(p_bar,1);
end
