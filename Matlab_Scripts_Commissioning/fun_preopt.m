function [y] = fun_preopt(x, BPMC_x, BPMC_preopt, BPMC_energies, IDD)


    % Gaussian weights
    gw = my_normpdf(BPMC_energies, x(1), x(2));
    
    % Compute weigthed MC dose
    SimDose = zeros(size(BPMC_preopt,1),1);
    for i=1:size(BPMC_preopt,2)
        SimDose = SimDose + gw(i)*BPMC_preopt(:,i);
    end
    MCdose = interp1(BPMC_x, SimDose, IDD(:,1), 'linear', 'extrap');
    clearvars SimDose;
    SimDose = MCdose./trapz(IDD(:,1),MCdose);

    % r80 relative error
    ind_before = find(SimDose>=max(SimDose)*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_simu = interp1([SimDose(ind_after); SimDose(ind_before)], [IDD(ind_after,1); IDD(ind_before,1)], max(SimDose)*80/100);
    ind_before = find(IDD(:,2)>=max(IDD(:,2))*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_meas = interp1([IDD(ind_after,2); IDD(ind_before,2)], [IDD(ind_after,1); IDD(ind_before,1)], max(IDD(:,2))*80/100);
    errel = (r80_meas-r80_simu);
    
    % r20 relative error
    ind_before = find(SimDose>=max(SimDose)*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_simu = interp1([SimDose(ind_after); SimDose(ind_before)], [IDD(ind_after,1); IDD(ind_before,1)], max(SimDose)*20/100);
    ind_before = find(IDD(:,2)>=max(IDD(:,2))*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_meas = interp1([IDD(ind_after,2); IDD(ind_before,2)], [IDD(ind_after,1); IDD(ind_before,1)], max(IDD(:,2))*20/100);
    errel20 = (r20_meas-r20_simu);
    
    % Dose to peak difference (%)
    dtpd = (max(IDD(:,2)) - max(SimDose))*100/max(IDD(:,2));
    
    % Mean pt-to-pt difference (%)
    L = r80_meas - IDD(1,1);
    mptpd = 0;
    for j=1:length(IDD(:,2))
       if (IDD(j,2) > max(IDD(:,2))*0.5/100)
          mptpd = mptpd + (abs(IDD(j,2) - SimDose(j))/IDD(j,2))*((IDD(j+1,1) - IDD(j,1))/L)*100; 
       end
    end
    
    % FWHM but at t%
    t = 60;
        
    ind_before_r = find(IDD(:,2)>=max(IDD(:,2))*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(IDD(:,2)>=max(IDD(:,2))*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_m = interp1([IDD(ind_after_r,2);IDD(ind_before_r,2)], [IDD(ind_after_r,1);IDD(ind_before_r,1)], max(IDD(:,2))*t/100);
    l_m = interp1([IDD(ind_before_l,2);IDD(ind_after_l,2)], [IDD(ind_before_l,1);IDD(ind_after_l,1)], max(IDD(:,2))*t/100);
    FWHM_meas = r_m-l_m;

    ind_before_r = find(SimDose>=max(SimDose)*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(SimDose>=max(SimDose)*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_s = interp1([SimDose(ind_after_r);SimDose(ind_before_r)], [IDD(ind_after_r,1);IDD(ind_before_r,1)], max(SimDose)*t/100);
    l_s = interp1([SimDose(ind_before_l);SimDose(ind_after_l)], [IDD(ind_before_l,1);IDD(ind_after_l,1)], max(SimDose)*t/100);
    FWHM_MC = r_s-l_s;
    
    err_FWHM = (FWHM_meas - FWHM_MC);

    % Objective function
    y = double(10*(errel^2) + errel20^2 + (1.2*err_FWHM-dtpd)^2);
       
    
end
