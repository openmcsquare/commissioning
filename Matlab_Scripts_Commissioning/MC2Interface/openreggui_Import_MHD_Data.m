function [ Data_info, Data_data ] = openreggui_Import_MHD_Data( FileName )

  [CurrentPath,~,~] = fileparts(mfilename('fullpath'));
  addpath([CurrentPath '/lib']);
  
  if(exist(FileName) == 0)
      error(['File ' FileName ' not found!'])
  end
  
  % Read MHD dose file
  disp(['Read MHD file: ' FileName]);
  Data_info = openreggui_mha_read_header(FileName);
  Data_data = openreggui_mha_read_volume(Data_info);
  
  % Convert data for compatibility with MCsquare
  % These transformations may be modified in a future version
  Data_data = flipdim(Data_data, 1);
  Data_data = flipdim(Data_data, 2);
  Data_data = permute(Data_data, [2 1 3]);


end

