function  MC2_Config = openreggui_Generate_MC2_Config( WorkDir, NumberOfPrimaries, CT, Plan, ScannerDirectory, BDL_File )

  if(nargin < 5)
      ScannerDirectory = 'home/marie/Documents/Doct/MCsquare/MCsquare/Scanners/UCL_Toshiba';
      BDL_File = 'home/marie/Documents/Doct/MCsquare/MCsquare/BDL/TRENTO_GTR2.txt';
  elseif(nargin < 6)
      BDL_File = 'home/marie/Documents/Doct/MCsquare/MCsquare/BDL/TRENTO_GTR2.txt';
  end


  curDir = pwd;
%   cd(WorkDir)
  cd(ScannerDirectory)
  ScannerDirectory = pwd;
  [file_path, file_name, file_ext] = fileparts(BDL_File);
  cd(curDir)
%   cd(WorkDir)
  cd(file_path)
  BDL_File = fullfile(pwd, [file_name file_ext]);
  cd(curDir)


  MC2_Config.WorkDir = WorkDir;

  % Simulation parameters
  MC2_Config.NumberOfThreads = 0;
  MC2_Config.RNG_seed = 0;
  MC2_Config.NumberOfPrimaries = NumberOfPrimaries;
  MC2_Config.E_Cut_Pro = 0.5;
  MC2_Config.D_Max = 0.2;
  MC2_Config.Epsilon_Max = 0.25;
  MC2_Config.Te_Min = 0.05;

  % Input files
  MC2_Config.CT = CT;
  MC2_Config.ScannerDirectory = ScannerDirectory;
  MC2_Config.BDL_File = BDL_File;
  MC2_Config.Plan = Plan;

  % Physical parameters
  MC2_Config.Simulate_Nuclear_Interactions = 1;
  MC2_Config.Simulate_Secondary_Protons = 1;
  MC2_Config.Simulate_Secondary_Deuterons = 1;
  MC2_Config.Simulate_Secondary_Alphas = 1;

  % 4D simulation
  MC2_Config.Simu_4D_Mode = 0;
  MC2_Config.Dose_4D_Accumulation = 0;
  MC2_Config.Field_type = 'Velocity';
  MC2_Config.Create_Ref_from_4DCT = 0;
  MC2_Config.Create_4DCT_from_Ref = 0;
  MC2_Config.Dynamic_delivery = 0;
  MC2_Config.Breathing_period = 7.0;

  % Robustness simulation
  MC2_Config.Robustness_Mode = 0;
  MC2_Config.ScenarioSelection = 'All';
  MC2_Config.Robust_Compute_Nominal = 1;
  MC2_Config.Robust_Systematic_Setup = [0.25 0.25 0.25];
  MC2_Config.Robust_Random_Setup = [0.1  0.1  0.1];
  MC2_Config.Robust_Range_Error = 3.0;
  MC2_Config.Robust_Systematic_Amplitude = 5.0;
  MC2_Config.Robust_Random_Amplitude = 5.0;
  MC2_Config.Robust_Systematic_Period = 5.0;
  MC2_Config.Robust_Random_Period = 5.0;

  % Beamlet simulation
  MC2_Config.Beamlet_Mode = 0;
  MC2_Config.Beamlet_Parallelization = 0;

  % Output parameters
  MC2_Config.Output_Directory = 'Outputs';
  MC2_Config.Out_Energy_ASCII = 0;
  MC2_Config.Out_Energy_MHD = 0;
  MC2_Config.Out_Energy_Sparse = 0;
  MC2_Config.Out_Dose_ASCII = 0;
  MC2_Config.Out_Dose_MHD = 1;
  MC2_Config.Out_Dose_Sparse = 0;
  MC2_Config.Out_LET_ASCII = 0;
  MC2_Config.Out_LET_MHD = 0;
  MC2_Config.Out_LET_Sparse = 0;
  MC2_Config.Out_Densities = 0;
  MC2_Config.Out_Materials = 0;
  MC2_Config.Compute_DVH = 0;
  MC2_Config.Dose_Sparse_Threshold = 0.0;
  MC2_Config.Energy_Sparse_Threshold = 0.0;
  MC2_Config.LET_Sparse_Threshold = 0.0;
  MC2_Config.PG_scoring = 0;
  MC2_Config.PG_LowEnergyCut = 0.0;
  MC2_Config.PG_HighEnergyCut = 50.0;
  MC2_Config.PG_Spectrum_NumBin = 150;
  MC2_Config.PG_Spectrum_Binning = 0.1;
  MC2_Config.LET_Method = 'StopPow';
  MC2_Config.Export_beam_dose = 0;
  MC2_Config.DoseToWater = 'Disabled';
  MC2_Config.Dose_Segmentation = 0;
  MC2_Config.Density_Threshold_for_Segmentation = 0.01;


  openreggui_Export_MC2_Config(MC2_Config);
end

