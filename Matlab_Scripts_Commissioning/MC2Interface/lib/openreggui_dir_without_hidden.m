function d = openreggui_dir_without_hidden(folder,type)

if(nargin<1)
   folder = pwd; 
end
if(nargin<2)
    type = 'all';
end

d = dir(folder);
switch type
    case 'folders'
        d = d(d.isdir);
end
is_hidden = zeros(1,length(d));

for i=1:length(is_hidden)    
    is_hidden(i) = strcmp(d(i).name(1),'.');
end

d(is_hidden > 0)=[];
