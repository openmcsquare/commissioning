function [mdata,all_energies,isoc_depth,IC_diameter,mask_begin,mask_end,nozzle_dist,format] = read_PristineBP(File,app)

if(nargin<5)
    app = [];
end

format = 'RayStation';
[~,~,fExt] = fileparts(File);

try
    mdata = textread(File,'%s','delimiter','\n');    
    %if contains(mdata{1},'RayStation') % RayStation format  
    if strcmp(fExt, '.csv')==1
        mask_begin = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Measured dose curve')));
        mask_end = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'End:')));
        mask_energy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Nominal beam energy')));
        mask_isodepth = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Isocenter to phantom surface distance')));
        mask_IC = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Detector lateral side')));
        mask_nozzle_dist = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'Isocenter to snout distance')));
    else % Eclipse format
        format = 'Eclipse';
        mask_begin = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'<')));
        dmb = diff(mask_begin)';
        ind = find(dmb>1);
        mask_begin = [mask_begin(1); mask_begin(ind+1)];
        mask_end = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'>')));
        dmb = diff(mask_end)';
        mask_end = [mask_end(dmb>1); mask_end(end)];
        mask_energy = find(~cellfun(@isempty,strfind(mdata(1:length(mdata)),'ENERGY')));
    end
catch
    if(not(isempty(app)))
        uialert(app.UIFigure,'Error in parsing input file.', 'Error');
    else
        disp('Error in parsing input file. Abort spot profile import.')
    end
    return
end

if(isempty(mask_energy))
    if(not(isempty(app)))
        uialert(app.UIFigure,'No pristine BP data found in this file. Check input file.', 'Error');
    else
        disp('No pristine BP data found. Abort import.')
    end
    return
elseif(length(mask_energy)~=length(mask_begin) || length(mask_energy)~=length(mask_end))
    if(not(isempty(app)))
        uialert(app.UIFigure,'Incorrect amount of data. Check input file.', 'Error');
    else
        disp('Incorrect amount of data. Abort pristine BP import.')
    end
    return
end

if strcmp(format,'RayStation') % RayStation format
    
    for i=1:length(mask_energy)
        all_energies(i) = str2double(regexprep(mdata(mask_energy(i)),'[^0-9.]',''));
        isoc_depth(i) = str2double(regexprep(mdata(mask_isodepth(i)),'[^0-9.]',''));
        IC_diameter(i) = str2double(regexprep(mdata(mask_IC(i)),'[^0-9.]',''));
        nozzle_dist(i) = str2double(regexprep(mdata(mask_nozzle_dist(i)),'[^0-9.]',''));
    end

else % Eclipse format
    
    for i=1:length(mask_energy)
        all_energies(i) = str2double(regexprep(mdata(mask_energy(i)),'[^0-9.]',''));
        isoc_depth = NaN;
        IC_diameter = NaN;
    end
    nozzle_dist = NaN;
    
end
