function [] = WriteToBDL(BDLname, energies, columns, data, RS)

if(nargin<5)
    RS = [];
end

% Read the BDL
A = textread(BDLname,'%s','delimiter','\n');
A{length(A)+1} = -1;
mask_rs = find(~cellfun(@isempty,strfind(A(1:length(A)-1),'Range Shifter parameters')));
mask_header = find(~cellfun(@isempty,strfind(A(1:length(A)-1),'NominalEnergy')));
BDL_parameters = importdata(BDLname, ' ', mask_header);

% Write new data at the right place in BDL data
for i=1:length(energies)
    BDL_parameters.data(BDL_parameters.data(:,1)==energies(i), columns) = data(i,:);
end

% Add RS
if(not(isempty(RS)))
    mask_beam = find(~cellfun(@isempty,strfind(A(1:length(A)-1),'Beam parameters')));
    A(mask_beam+7:end+7) = A(mask_beam:end);
    mask_rs = mask_beam;
    mask_header = mask_header + 7;
    A{mask_rs} = 'Range Shifter parameters';
    A{mask_rs+1} = ['RS_ID = ',RS.ID];
    A{mask_rs+2} = ['RS_type = ',RS.type];
    A{mask_rs+3} = ['RS_material = ',RS.material];
    A{mask_rs+4} = ['RS_density = ',RS.density];
    A{mask_rs+5} = ['RS_WET = ',RS.WET];
    A{mask_rs+6} = '';
end

% Convert data to string
for i=mask_header+1:length(A)-1
    A{i} = num2str(BDL_parameters.data(i-mask_header,:));
end

% Write to BDL
f=fopen(BDLname,'w');
for k = 1:numel(A)
    if A{k+1} == -1
        fprintf(f,'%s', A{k});
        break;
    else
        fprintf(f,'%s\n', A{k});
    end
end
fclose(f);

end

