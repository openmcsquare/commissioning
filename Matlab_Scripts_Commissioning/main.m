close all
clear all
clc

format long e
t_start = clock;

global use_gui;
global use_debug;

% Path
code_dir = fileparts(mfilename('fullpath')); % path to main.m
MCsquareDir = fullfile(fileparts(code_dir),'MCsquare'); % path to MCsquare/MCsquare
addpath(code_dir, fullfile(code_dir,'MC2Interface')); % path to MCsquare/MC2interface

%% To be modified by user 

% Running mode
use_gui = 0; % display figures (1 or 0)
use_debug = 0; % for debugging (1 or 0)
optimize = 1; % optimize BDL (1) or only evaluate (0)
speed_mode_ES = 0; % 1 for fast, 0 for accurate
speed_mode_AD = 1; % 1 to simulate one beamlet and generate other via rotation, 0 for simulating the whole field (absolute dose tuning)

% Data input and output
DataDir = fullfile(code_dir, '..', 'Data', 'Commissioning_measurements'); % path to measurements folder
BDLname = fullfile(MCsquareDir, 'BDL', 'Sample_BDL.txt'); % name of the BDL (must not exist yet)
energy_n = []; % list of energies to commission among the whole list of measurements, or [] if all energies given in measurement file are wanted, or energy binning if single value smaller than minimum energy in measurement file

% Machine info
nozzle2iso = 400; % distance between nozzle and isocenter, in mm
focalx = 2000; % SADX in mm
focaly = 2000; % SADY in mm

% Measurements info: phase space
PS_meas = fullfile(DataDir, 'RayStation_sample', 'Measured_SpotProfiles.csv');
% PS_meas = fullfile(DataDir, 'Eclipse_old_sample', 'spot_profiles.asc');
%PS_meas = {fullfile(DataDir, 'Eclipse_new_sample', 'Measured_Spot_Fluence_Profile_X.txt'), fullfile(DataDir, 'Eclipse_new_sample', 'Measured_Spot_Fluence_Profile_Y.txt')}; % cell array of files, or just one file
flag_DG = 0; % flag for use of double gaussian in the modeling (1 for double, 0 for single)

% Measurements info: energy spectrum
CT_ES = fullfile(fileparts(mfilename('fullpath')),'CT','CT.mhd'); % CT file, can be mhd or the first slice of dcm sequence; if mhd, the raw file must be in the same folder
IDD = fullfile(DataDir, 'RayStation_sample', 'Measured_PristineBraggPeaks.csv'); % name of IDD measurements file
% IDD = fullfile(DataDir, 'Eclipse_old_sample', 'IDD_sample.asc'); 
%IDD = fullfile(DataDir, 'Eclipse_new_sample', 'Measured_Depth_Dose_Parallel_Plate.txt'); 
simu_spacing_ES = [2 0.5 2]; % Spatial resolution (in mm) for the MC simulation (second component is depth)
Isocenter_depth_ES = 0.; % only for Eclipse measurements: depth of isocenter during measurements
integration_diameter = 120.; % only for Eclipse measurements: diameter of IC used for measurements (or integration diameter if simulations instead of measurements)

% Measurements info: absolute dose
CT_AD = CT_ES; % CT file, can be the dcm or mhd; if mhd, the raw file must be in the same folder (CT must be large enough to allow the measured field to be simulated)   
AD_meas = fullfile(DataDir, 'RayStation_sample', 'Measured_AbsoluteDosimetry.csv');
% AD_meas = fullfile(DataDir, 'Eclipse_old_sample', 'IDD_sample.asc');
%AD_meas = fullfile(DataDir, 'Eclipse_new_sample', 'Measured_Depth_Dose_Parallel_Plate.txt');
detector.SensitiveVolumeSize = 9.9; % Diameter (for circle) or width (for square) of the sensitive volume of ionization chamber used, in mm (for instance 9.9 for PPC05)
detector.Shape = 'circle'; % Shape of the ionization chamber used: 'circle' or 'square' (for instance, PPC05 is circle)
simu_spacing_AD = [1 1 1]; % spacing wanted for MC simulations, second component is depth
RBE_factor = 1; % 1.1 if measured dose is physical, 1 if it is biological
Calibration_depth = 20.; % only for Eclipse measurements: depth of point used for absolute dose calibration 
Isocenter_depth = 0.; % only for Eclipse measurements: depth of isocenter during measurements
Field_size = [0. 0.]; % only for Eclipse measurements: size of measured field in x and y, in mm ([0 0] for single spot)
Spots_spacing = [1. 1.]; % only for Eclipse measurements: measurements spots spacing in x and y, in mm ([1 1] for single spot)
SAD_factor = 1.; % only for Eclipse measurements: the SAD_factor multiplies the initially measured dose and is defined as focalx*focaly/(focalx - Distance_Iso_Monitor)/(focaly - Distance_Iso_Monitor); set to 1 if not used (a.u.)
IC_correction_factor = 1.; % only for Eclipse measurements: the factor applied to the measured dose to correct for the size difference of IC used for IDD integration and absolute dose calibration (a.u.)


%% BDL creation
if(exist(BDLname,'file') && optimize)
    disp('BLD already exists. Backup and continue.')
    copyfile(BDLname,strrep(BDLname,'.txt','_bkp.txt'));
end

if(optimize || not(exist(BDLname,'file')))
    disp(['Create BDL template: ' BDLname])
    create_BDL(BDLname, energy_n, nozzle2iso, focalx, focaly, AD_meas);
end


%% Phase space tuning
if(optimize)
    disp('Optimization of phase space parameters')
    tune_PS(BDLname, nozzle2iso, PS_meas, flag_DG);
end


%% Energy spectrum tuning
if(optimize)
    disp('Optimization of energy spectrum parameters')
    tune_ES(MCsquareDir, BDLname, CT_ES, IDD, simu_spacing_ES, speed_mode_ES, Isocenter_depth_ES, integration_diameter, nozzle2iso);
end


%% Protons per MU tuning
if(optimize)
    disp('Optimization of protons per MU')
    tune_AD(MCsquareDir, BDLname, CT_AD, AD_meas, simu_spacing_AD, detector, speed_mode_AD, focalx, focaly, RBE_factor, Calibration_depth, Isocenter_depth, Field_size, Spots_spacing, SAD_factor, IC_correction_factor, nozzle2iso);
end

if(optimize)
    disp('Commissioning complete.')
    disp(['Processing time = ',num2str(etime(clock,t_start)/60/60),' h'])
end


%% Visualization
if(use_gui && optimize==0)
    display_BDL_vs_data(BDLname, PS_meas, nozzle2iso);
end

