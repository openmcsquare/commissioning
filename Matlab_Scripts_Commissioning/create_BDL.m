function [] = create_BDL(BDLname, nominal_energies, nozzle2iso, fx, fy, MeasFile)


[Directory,~,~] = fileparts(BDLname);
if(~exist(Directory))
  mkdir(Directory)
end

[~,~,ext] = fileparts(MeasFile); % file extension
datameas = textread(MeasFile,'%s','delimiter','\n');

% Measurements, to find all nominal energies

if strcmp(ext, '.csv') % RayStation format
    mask_before = find(~cellfun(@isempty,strfind(datameas(1:length(datameas)),'Measurement (Nominal')));
    mask_after = find(~cellfun(@isempty,strfind(datameas(1:length(datameas)),'End:')));
    data = regexprep(datameas(mask_before(end)+1:mask_after(end)-1),'[^a-zA-Z0-9.]',' ');
    for l=1:size(data,1)
        Data(l,:) = str2double(strsplit(data{l},' '));
    end
else % Eclipse format
    mask_energy = find(~cellfun(@isempty,strfind(datameas(1:length(datameas)),'ENERGY')));
    for i=1:length(mask_energy)
        Data(i,1) = str2double(regexprep(datameas(mask_energy(i)),'[^0-9.]',''));
    end
end

if(isempty(nominal_energies))
    nominal_energies = Data(:,1); % in MeV
elseif(length(nominal_energies)==1 && nominal_energies<min(Data(:,1)))
    nominal_energies = [min(Data(:,1)):nominal_energies:max(Data(:,1))]';
    if(nominal_energies(end)<max(Data(:,1)))
        nominal_energies = [nominal_energies;max(Data(:,1))];
    end
end

nominal_energies = unique(nominal_energies);
l = length(nominal_energies);

% Write initial BDL
fid = fopen(BDLname, 'w');
fprintf(fid, '--UPenn beam model (double gaussian)--\n# Beam data library for P1\n\nNozzle exit to Isocenter distance\n%3.2f\n\nSMX to Isocenter distance\n%4.2f\n\nSMY to Isocenter distance\n%4.2f\n\nBeam parameters\n%d energies\n\nNominalEnergy\tMeanEnergy\tEnergySpread\tProtonsMU\tWeight1\tSpotSize1x\tDivergence1x\tCorrelation1x\tSpotSize1y\tDivergence1y\tCorrelation1y\tWeight2\tSpotSize2x\tDivergence2x\tCorrelation2x\tSpotSize2y\tDivergence2y\tCorrelation2y\n', nozzle2iso, fx, fy, l);
data = zeros(l,18);
data(:,1) = nominal_energies;
data(:,2) = nominal_energies;
data(:,3) = 0.5*ones(l,1);
data(:,4) = 100000000*ones(l,1);
data(:,5) = ones(l,1);
data(:,6) = 5*ones(l,1);
data(:,7) = 0.003*ones(l,1);
data(:,8) = 0.2*ones(l,1);
data(:,9) = 5*ones(l,1);
data(:,10) = 0.003*ones(l,1);
data(:,11) = 0.2*ones(l,1);
data(:,12) = zeros(l,1);
data(:,13) = 5*ones(l,1);
data(:,14) = 0.003*ones(l,1);
data(:,15) = 0.2*ones(l,1);
data(:,16) = 5*ones(l,1);
data(:,17) = 0.003*ones(l,1);
data(:,18) = 0.2*ones(l,1);
fprintf(fid, '%3.3f %3.6f %1.6f %9.1f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f %1.6f\n', data');
fclose(fid);

end

