function [] = tune_ES(MCsquareDir, BDL, CT, MeasFile, new_spacing, speed_mode, Isocenter_depth_ES, IC_diameter_ES, nozzle_exit, p_bar)

global use_gui;
global use_debug;
global SimulatedProtons;

if(nargin<10)
    p_bar = [];
end

%% Data

% Work directory for MCsquare
WorkDir = fullfile(MCsquareDir, '..', 'WorkMC');
if(~exist(WorkDir))
    mkdir(WorkDir);
end
s1 = what(WorkDir);
p1 = s1.path;
clearvars s1;

% Resampling CT (modifying only MHD file because here only water)
[filepath,name,ext] = fileparts(CT);
s2 = what(filepath);
p2 = s2.path;
clearvars s2;
if ext=='.dcm'
    [CT_info, ~] = openreggui_DicomCT_to_MHD(CT, fullfile(WorkDir, 'CT.mhd'));
elseif ext=='.mhd'
    A = textread(CT,'%s','delimiter','\n');
    mask_size = find(~cellfun(@isempty,strfind(A(1:length(A)),'DimSize')));
    mask_sp = find(~cellfun(@isempty,strfind(A(1:length(A)),'ElementSpacing')));
    mask_ori = find(~cellfun(@isempty,strfind(A(1:length(A)),'Offset')));
    mask_raw = find(~cellfun(@isempty,strfind(A(1:length(A)),'ElementDataFile')));
    CT_info.Size = str2double(strsplit(strtrim(string(regexprep(A(mask_size(1)),'[^0-9.]',' '))),' '));
    CT_info.Spacing = str2double(strsplit(strtrim(string(regexprep(A(mask_sp(1)),'[^0-9.]',' '))),' '));
    CT_info.ImagePositionPatient = str2double(strsplit(strtrim(string(regexprep(A(mask_ori(1)),'[^-0-9.]',' '))),' '));
    CTraw = strtrim(strsplit(string(A(mask_raw)),'='));
    if strcmp(string(p1),string(p2))==0
        copyfile(CT, WorkDir);
        copyfile(fullfile(p2, char(CTraw(2))), WorkDir);
    end
else
    error('Error: please enter a valid CT name (dcm or mhd)');
end
resolution_fact = CT_info.Spacing(:)./new_spacing(:);
mhdCT = fopen(fullfile(WorkDir, 'CT.mhd'), 'r');
i = 1;
tline = fgetl(mhdCT);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(mhdCT);
    A{i} = tline;
end
new_size = [round(CT_info.Size(1)*resolution_fact(1)), round(CT_info.Size(2)*resolution_fact(2)), round(CT_info.Size(3)*resolution_fact(3))];
A{3} = sprintf('DimSize = %d %d %d', new_size(1), new_size(2), new_size(3));
A{4} = sprintf('ElementSpacing = %f %f %f', new_spacing(1), new_spacing(2), new_spacing(3));
fclose(mhdCT);

% Rewrite mhd CT
f=fopen(fullfile(WorkDir, 'CT.mhd'), 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(f,'%s', A{i});
        break;
    else
        fprintf(f,'%s\n', A{i});
    end
end
fclose(f);
clearvars A;

% Measurements data
[mdata,all_energies,isoc_depth,IC_diameter,mask_begin,mask_end,nozzle_dist,data_fmt] = read_PristineBP(MeasFile);
if(isnan(isoc_depth(1)))
    isoc_depth = Isocenter_depth_ES(:).*ones(length(all_energies),1);
end
if(isnan(IC_diameter(1)))
    IC_diameter = IC_diameter_ES(:).*ones(length(all_energies),1);
end
if(isnan(nozzle_dist(1)))
    nozzle_dist = nozzle_exit(1)*ones(length(all_energies),1);
end

% Read BDL
A = textread(BDL,'%s','delimiter','\n');
mask_header = find(~cellfun(@isempty,strfind(A(1:length(A)),'NominalEnergy')));
BDL_parameters = importdata(BDL, ' ', mask_header);

% CT
origin = CT_info.ImagePositionPatient;
true_centre = origin(:) + (new_size(:)-1).*new_spacing(:)/2 + new_spacing(:)/2;
centre = true_centre(:) - origin(:);
centre(2) = (new_size(2)-1)*new_spacing(2) - isoc_depth(1) + new_spacing(2)/2;

% Vectors, grids
v1 = origin(1):new_spacing(1):(origin(1) + (new_size(1)-1)*new_spacing(1));
v2 = origin(2):new_spacing(2):(origin(2) + (new_size(2)-1)*new_spacing(2));
v3 = origin(3):new_spacing(3):(origin(3) + (new_size(3)-1)*new_spacing(3));
v1 = v1 + new_spacing(1)/2;
v2 = v2 + new_spacing(2)/2;
v3 = v3 + new_spacing(3)/2;
[X,Y,Z] = meshgrid(v1, v2, v3);


%% Optimization for each energy of the ES

energybdl = BDL_parameters.data(:,1);

h = figure;
if use_gui==0
    set(h, 'Visible', 'off');
end
colors = hsv(length(energybdl));

if (abs(nozzle_dist-420) <= abs(nozzle_dist-500))
    BP_MC = load('BPLibrary_50to280_420.mat');
    if (abs(nozzle_dist-420)>=50 & speed_mode == 1)
        disp('WARNING: you should consider switching to accurate mode')
    end
else
    BP_MC = load('BPLibrary_50to280_500.mat');
    if (abs(nozzle_dist-500)>=50 & speed_mode == 1)
        disp('WARNING: you should consider switching to accurate mode')
    end
end
[~,ind_ICd] = min(abs(IC_diameter(1) - [81.6 120 200]));
BPMC_x = BP_MC.MCdose(:,1, ind_ICd);
BPMC_y = BP_MC.MCdose(:,2:end, ind_ICd);
BPMC_energies = 50:0.05:280;
fidd = fopen('results_ES.txt','w');

for num_energ=1:length(energybdl)
    
    if(not(isempty(p_bar)))
        commissioning_progress(p_bar,(num_energ-1)/length(energybdl));
    end
    
    % Measured data
    ind1 = find(round(all_energies*1000)/1000==energybdl(num_energ));
    if(strcmp(data_fmt, 'RayStation')) % RayStation format
        idd = regexprep(mdata(mask_begin(ind1(1))+1:mask_end(ind1(1))-1),'[^-0-9.eE]',' ');
        for l=1:size(idd,1)
            if(length(str2double(split(strtrim(idd{l}))))>2)
               disp(str2double(split(strtrim(idd{l})))) 
            end
            IDD(l,:) = str2double(split(strtrim(idd{l})));
        end
    else
        idd = regexprep(mdata(mask_begin(ind1(1)):mask_end(ind1(1))),'[<>]','');
        for l=1:size(idd,1)
            IDD(l,:) = str2double(strsplit(strtrim(idd{l}),' '));
        end
        tmpind = find(abs(IDD(1,1:3)-IDD(2,1:3))>1e-4);
        IDD = IDD(:, [tmpind 4]);
    end
    IDD = sortrows(IDD,1);
    delta = IDD(end,1) -  IDD(end-1,1);
    supp = [IDD(end,1):delta:IDD(end,1)+30]';
    IDD = [IDD ; [supp zeros(size(supp))]];
    IDD(IDD(:,1)<new_spacing(2)/2,:) = [];
    IDD_sum(:,1) = IDD(:,1);
    IDD_sum(:,2) = IDD(:,2)./trapz(IDD(:,1), IDD(:,2)); % IDD normalized to sum
    
    
    % r80 and theoretical mean energy for measured data
    ind_before = find(IDD_sum(:,2)>=max(IDD_sum(:,2))*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_meas = interp1([IDD_sum(ind_after,2); IDD_sum(ind_before,2)], [IDD_sum(ind_after,1); IDD_sum(ind_before,1)], max(IDD_sum(:,2))*80/100);
    E_0 = exp(3.464048+0.561372013*log(r80_meas/10) - 0.004900892.*(log(r80_meas/10)).^2 + 0.001684756748*(log(r80_meas/10)).^3);
    
    % Pre-optimization with MC peaks library    
    BPMC_preopt = BPMC_y(:,BPMC_energies >= energybdl(num_energ)-7 & BPMC_energies <= energybdl(num_energ)+7);
    x0 = double([E_0 0.1]);
    ub = double([E_0+4 3]);
    lb = double([E_0-4 1e-2]);
    options = optimoptions('patternsearch','Display','iter','MaxIterations', 900000);
    funct = @(x) fun_preopt(x, BPMC_x, BPMC_preopt, BPMC_energies(BPMC_energies >= energybdl(num_energ)-7 & BPMC_energies <= energybdl(num_energ)+7), IDD_sum);
    [sol_e(num_energ,:),fval_e(num_energ,:),exitflag_e(num_energ)] = patternsearch(funct,x0,[],[],[],[],lb,ub,[],options);
    x0(1,1) = double(sol_e(num_energ,1));
    x0(1,2) = double(sol_e(num_energ,2)/energybdl(num_energ)*100);
    sol_e(num_energ,:) = x0;
    
    % Create plan
    centre(2) = (new_size(2)-1)*new_spacing(2) - isoc_depth(ind1(1)) + new_spacing(2)/2;
    fid = fopen(fullfile(WorkDir, 'OneSpot.txt'), 'w');
    fprintf(fid, '#TREATMENT-PLAN-DESCRIPTION\n#PlanName\nOneSpot\n#NumberOfFractions\n1\n##FractionID\n1\n##NumberOfFields\n1\n###FieldsID\n1\n#TotalMetersetWeightOfAllFields\n1\n\n#FIELD-DESCRIPTION\n###FieldID\n1\n###FinalCumulativeMeterSetWeight\n1\n###GantryAngle\n0\n###PatientSupportAngle\n0\n###IsocenterPosition\n%f\t%f\t%f\n###NumberOfControlPoints\n1\n\n#SPOTS-DESCRIPTION\n####ControlPointIndex\n1\n####SpotTunnedID\n1\n####CumulativeMetersetWeight\n1\n####Energy (MeV)\n%f\n####NbOfScannedSpots\n1\n####X Y Weight\n0 0 1', centre(1), centre(2), centre(3), energybdl(num_energ));
    fclose(fid);
    
    % Optimization of both parameters
    if (speed_mode == 0)
        SimulatedProtons = 1e7;
        lb = double([E_0-4, 0]);
        ub = double([E_0+4, 3]);
        funct = @(x) fun_ES(x, IDD_sum, WorkDir, new_spacing, new_size, origin, true_centre, IC_diameter(ind1(1)), energybdl(num_energ), BDL, MCsquareDir);
        if(use_debug)
            options = optimset('Display', 'iter', 'TolX', 1, 'TolFun', 1, 'MaxFunEvals', 1);
        else
            options = optimset('Display', 'iter', 'TolX', 1e-3, 'TolFun', 5e-2, 'MaxFunEvals', 120);
        end
        [sol_e(num_energ,:),fval_e(num_energ,:),exitflag_e(num_energ),output_e] = fminsearch(funct,x0,options)
        if (fval_e(num_energ) > 1e-2 && not(use_debug))
            SimulatedProtons = 8e6;
            options = optimoptions('patternsearch','StepTolerance', 1e-4, 'Cache', 'on', 'Display', 'iter', 'MeshTolerance', 1e-4, 'OutputFcn', @StopCriterion, 'TolFun', 1e-6, 'TolX', 5e-5);
            [sol_e_ps(num_energ,:),fval_e_ps(num_energ,:),exitflag_e(num_energ),output_e] = patternsearch(funct,x0,[],[],[],[],lb,ub,[],options);
            flag_ps(num_energ) = 1;
            if (fval_e_ps(num_energ)<fval_e(num_energ))
                sol_e(num_energ,:) = sol_e_ps(num_energ,:);
                fval_e(num_energ,:) = fval_e_ps(num_energ);
            end
        end
    end
    
    % Write solution to BDL
    WriteToBDL(BDL, energybdl(num_energ), [2 3], sol_e(num_energ,:));
    
    % MC simu
    SimulatedProtons = 5e6;
    openreggui_Generate_MC2_Config(WorkDir, SimulatedProtons, 'CT.mhd', 'OneSpot.txt', fullfile(MCsquareDir, 'Scanners/Water_only'), BDL);
    disp(' ');
    disp('Start MCsquare simulation');
    if(ispc)
        copyfile(fullfile(MCsquareDir, 'Materials'), fullfile(WorkDir, 'Materials'));
        cd(WorkDir)
        eval(['!',fullfile(MCsquareDir, 'MCsquare.bat >> log.txt 2>>&1')]);
    else
        system(['dos2unix ',fullfile(MCsquareDir, 'MCsquare_double')]);
        system(['cd ' WorkDir ' && ' fullfile(MCsquareDir, 'MCsquare_double')]);
    end
    [~, Dose_data] = openreggui_Import_MHD_Data(fullfile(WorkDir, 'Outputs/Dose.mhd'));
    
    % Computation of MC depth dose
    Dose_data(sqrt((X-true_centre(1)).^2 + (Z-true_centre(3)).^2) > IC_diameter(ind1(1))/2) = 0;
    Dosesum = squeeze(sum(sum(Dose_data, 2),3));
    absci = 0:length(Dosesum)-1;
    absci = absci.*new_spacing(2) + new_spacing(2)/2;
    MCdose = interp1(absci, Dosesum, IDD(:,1), 'linear', 'extrap');
    MCdose(MCdose<0) = 0;
    MCdose = MCdose./trapz(IDD(:,1), MCdose);
    
    % Evaluation of optimization with 4 metrics
    
    % r80 relative error
    ind_before = find(MCdose>=max(MCdose)*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_simu = interp1([MCdose(ind_after); MCdose(ind_before)], [IDD_sum(ind_after,1); IDD_sum(ind_before,1)], max(MCdose)*80/100);
    ind_before = find(IDD_sum(:,2)>=max(IDD_sum(:,2))*80/100, 1, 'last');
    ind_after = ind_before + 1;
    r80_meas = interp1([IDD_sum(ind_after,2); IDD_sum(ind_before,2)], [IDD_sum(ind_after,1); IDD_sum(ind_before,1)], max(IDD_sum(:,2))*80/100);
    errange(num_energ) = (r80_meas-r80_simu)
    
    % r20 relative error
    ind_before = find(MCdose>=max(MCdose)*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_simu = interp1([MCdose(ind_after); MCdose(ind_before)], [IDD_sum(ind_after,1); IDD_sum(ind_before,1)], max(MCdose)*20/100);
    ind_before = find(IDD_sum(:,2)>=max(IDD_sum(:,2))*20/100, 1, 'last');
    ind_after = ind_before + 1;
    r20_meas = interp1([IDD_sum(ind_after,2); IDD_sum(ind_before,2)], [IDD_sum(ind_after,1); IDD_sum(ind_before,1)], max(IDD_sum(:,2))*20/100);
    errel20(num_energ) = (r20_meas-r20_simu)
    
    % FWMH at t% dose
    t = 60;
    ind_before_r = find(IDD_sum(:,2)>=max(IDD_sum(:,2))*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(IDD_sum(:,2)>=max(IDD_sum(:,2))*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_m = interp1([IDD_sum(ind_after_r,2);IDD_sum(ind_before_r,2)], [IDD_sum(ind_after_r,1);IDD_sum(ind_before_r,1)], max(IDD_sum(:,2))*t/100);
    l_m = interp1([IDD_sum(ind_before_l,2);IDD_sum(ind_after_l,2)], [IDD_sum(ind_before_l,1);IDD_sum(ind_after_l,1)], max(IDD_sum(:,2))*t/100);
    FWHM_meas = r_m-l_m;
    ind_before_r = find(MCdose>=max(MCdose)*t/100, 1, 'last');
    ind_after_r = ind_before_r+1;
    ind_after_l = find(MCdose>=max(MCdose)*t/100, 1, 'first');
    ind_before_l = ind_after_l-1;
    r_s = interp1([MCdose(ind_after_r);MCdose(ind_before_r)], [IDD_sum(ind_after_r,1);IDD_sum(ind_before_r,1)], max(MCdose)*t/100);
    l_s = interp1([MCdose(ind_before_l);MCdose(ind_after_l)], [IDD_sum(ind_before_l,1);IDD_sum(ind_after_l,1)], max(MCdose)*t/100);
    FWHM_MC = r_s-l_s;
    err_FWHM(num_energ) = FWHM_meas - FWHM_MC
    
    % Dose to peak difference (%)
    dtpd(num_energ) = (max(IDD_sum(:,2)) - max(MCdose))*100/max(IDD_sum(:,2))
    
    % Mean point to point difference
    L = r80_meas - IDD_sum(1,1);
    mptpd(num_energ) = 0;
    for j=1:length(IDD_sum(:,2))-1
        if (IDD_sum(j,2) > max(IDD_sum(:,2))/40)
            mptpd(num_energ) = mptpd(num_energ) + (abs(IDD_sum(j,2) - MCdose(j))/IDD_sum(j,2))*((IDD_sum(j+1,1) - IDD_sum(j,1))/L)*100;
        end
    end
    
    % Objective function value
    y(num_energ) = double(10*(errange(num_energ)^2) + errel20(num_energ)^2 + (1.2*err_FWHM(num_energ)-dtpd(num_energ))^2)
    
    % Write metrics values in file
    fprintf(fidd, '%f\t%f\t%f\t%f\t%f\n', energybdl(num_energ), errange(num_energ), errel20(num_energ), err_FWHM(num_energ), dtpd(num_energ));
    
    % Plot of Bragg peaks simulated and measured
    subplot(1,2,1)
    hold on;
    plot(IDD_sum(:,1), IDD_sum(:,2),'.', 'color', colors(num_energ,:));
    hold on;
    plot(IDD_sum(:,1), MCdose, 'color', colors(num_energ,:));
    xlabel('Depth (mm)');
    ylabel('Relative dose (a.u.)');
    legend('Measurements','MC simulation');
    
    clearvars idd IDD IDD_sum IDD_max MCdose Dose_info Dose_data fval Dosesum1 Dosesum2 vect_z absci;
    
end

fclose(fidd);

% Plot for metrics evaluation
subplot(1,2,2)
plot(energybdl, errange, '*-', energybdl, errel20, '-o', energybdl, dtpd, '-.', energybdl, err_FWHM, 'd-')
xlabel('Energy (MeV)')
ylabel('Error')
legend('Range 80 difference (mm)', 'Range 20 difference (mm)', 'Dose to peak difference (%)', 'BP width difference (mm)');
saveas(h,'ES_evaluation.fig');

if(not(isempty(p_bar)))
    commissioning_progress(p_bar,1);
end
