clear all
close all
clc

format long e
code_dir = fileparts(mfilename('fullpath')); % path to main.m$


%% Input data - To be modified by user

% Graphical outputs
use_gui = 1; % 0 for no graphical output, 1 for main outputs, 2 for detailed outputs

% Data directory
DataDir = fullfile(code_dir, '..', 'Data', 'CT_Calibration'); % path to folder with all files needed (phantom and ICRU tissues properties)

% MCsquare directory etc
MCsquareDir = fullfile(code_dir, '..', 'MCsquare'); % Path to MCsquare directory

% Phantom data - you can write down your measured HU in front of the corresponding insert name in the file PhantomHU.csv
% You can delete the lines of the inserts you didn't measure
% You can add lines for unlisted inserts but then need to define their corresponding properties in the file PhantomProperties.csv
PhantomHU = fullfile(DataDir, 'Gammex_phantom', 'PhantomMeasuredHU.csv');
PhantomProperties = fullfile(DataDir, 'Gammex_phantom', 'PhantomProperties.csv');

% Where to create the calibration curve
CT_CC_name = fullfile(MCsquareDir, 'Scanners', 'My_CT'); % Wanted name and location for the calibration curve


%% Schneider 1996 : calibration of coefficients K

% Load phantom data
HUTable = readtable(PhantomHU, 'Delimiter', ',', 'ReadVariableNames', true);
HU = HUTable{:,2};
HU = HU(:);
propertiesTable = readtable(PhantomProperties, 'Delimiter', ',', 'ReadVariableNames', true);
Z = propertiesTable{1,3:end};
A = propertiesTable{2,3:end};
propertiesTable = propertiesTable(3:end, :);
[~,HUOrder] = ismember(HUTable{:,1}, propertiesTable{:,1});
densities = propertiesTable{HUOrder,2};
densities = densities(:);
if max(densities)>100
    densities=densities/1000;
end
w = propertiesTable{HUOrder,3:end};
w = w./sum(w,2);

% Constants
rho_w = 1; % water density
avo = 6.02214076*1e23; % avogadro number
w_w = zeros(size(Z));
w_w(Z==1) = 0.1119;
w_w(Z==8) = 0.8881; % water compo

% Optimization of coefficients K
fct = @(x) fun_CT(x, Z, A, w, w_w, rho_w, HU, densities);
x0 = [1e-04 3e-05];
[sol,fval] = fminsearch(fct, x0);
if (sol(1)<0 | sol(2)<0 | sol(1)>1 | sol(2)>1)
    [sol,fval] = patternsearch(fct, x0, [], [], [], [], [1e-8 1e-8], [1 1]); % check bounds
end
if (fval>1e-1)
    disp('WARNING: Computed and measured HU probably don''t match so well')
end

% Computation of relative attenuation coefficients and corresponding HU for the found coefficients K
mu_w = sum(w_w./A.*Z.*(1 + Z.^1.86*sol(1) + Z.^3.62*sol(2)));
mu1 = (Z./A.*(1 + Z.^1.86*sol(1) + Z.^3.62*sol(2)))*w';
mu_calc = densities/rho_w.*mu1(:)/mu_w; % mu relative to water
hu_calc = (mu_calc-1)*1000;

% Plot calculated HU vs initial HU obtained with CT
if (use_gui>0)
    xx=min(hu_calc)-50:0.01:max(hu_calc)+50;
    figure;
    plot(HU, hu_calc, '*',xx,xx);
    xlabel('Measured HU');
    ylabel('Calculated HU');
    title('Calibration');
end


%% Read Schneider 2000 materials

% Read names and elemental compositions
SchneiderNames = importdata(fullfile(DataDir, 'Names_TissuesSchneider.txt'), '\n');
Compo = load(fullfile(DataDir, 'Compo_TissuesSchneider.txt'))/100;

% Find materials ID in list.dat
list = textread(fullfile(MCsquareDir, 'Materials', 'list.dat'), '%s','delimiter','\n');
for i=1:length(SchneiderNames)
    mask = find(~cellfun(@isempty,strfind(list(1:length(list)),SchneiderNames{i})));
    mats = strsplit(string(list(mask(1))),' ');
    mat_nb(i,1) = str2double(mats(1));
end


%% Calibration of Schneider 2000 for soft tissues, using previously found K

clearvars W rho ww;

% Data
Z = [1 6 7 8 11 12 15 16 17 19 20 26];
A = [1.008 12.011 14.007 15.999 22.99 24.305 30.974 32.060 35.453 39.098 40.078 55.845];
w_w = [0.1119 0 0 0.8881 0 0 0 0 0 0 0 0];

% Read info on tissues
W = load(fullfile(DataDir,'CompoWW_SoftTissues.txt'))/100;
rho = load(fullfile(DataDir, 'MassDensityWW_SoftTissues.txt'))/1000;
Names_ST = textread(fullfile(DataDir, 'NamesWW_SoftTissues.txt'), '%s', 'delimiter', '\n');

% Add air in data
W(end+1,:) = [0 0.012561 76.508170 23.479269 0 0 0 0 0 0 0 0]/100;
rho(end+1,1) = 0.00121;
W_ST_exact = W;
Names_ST{end+1} = 'air';

% Indices of needed tissues
Adipose3 = 3;
AdrenalGland = 4;
SmallIntestine = 15;
ConnectiveTissue = 12;
Lung = 28;

% HU of materials needed for interpolation
mu_w = rho_w*avo*sum(w_w./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)));
HU_SoftTissues(size(W,1),1) = 1000*(rho(end)*avo*sum(W(end,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);
HU_SoftTissues(Adipose3,1) = 1000*(rho(Adipose3)*avo*sum(W(Adipose3,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);
HU_SoftTissues(AdrenalGland,1) = 1000*(rho(AdrenalGland)*avo*sum(W(AdrenalGland,:)./A.*(Z + sol(2)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);
HU_SoftTissues(SmallIntestine,1) = 1000*(rho(SmallIntestine)*avo*sum(W(SmallIntestine,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);
HU_SoftTissues(ConnectiveTissue,1) = 1000*(rho(ConnectiveTissue)*avo*sum(W(ConnectiveTissue,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);

% Calibration
mu_w = rho_w*avo*sum(w_w./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)));
for i=1:size(W,1)
    mu_reftissues(i,1) = rho(i)*avo*sum(W(i,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)));
    HU_SoftTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1);
    if (HU_SoftTissues(i,1) <= HU_SoftTissues(AdrenalGland,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(Adipose3))
        Density_SoftTissues(i,1) = (rho(Adipose3)*HU_SoftTissues(AdrenalGland) - rho(AdrenalGland)*HU_SoftTissues(Adipose3) + (rho(AdrenalGland) - rho(Adipose3))*HU_SoftTissues(i))/(HU_SoftTissues(AdrenalGland) - HU_SoftTissues(Adipose3));
        W_SoftTissues(i,:) = rho(Adipose3)*(HU_SoftTissues(AdrenalGland) - HU_SoftTissues(i)) / (rho(Adipose3)*HU_SoftTissues(AdrenalGland) - rho(AdrenalGland)*HU_SoftTissues(Adipose3) + (rho(AdrenalGland)-rho(Adipose3))*HU_SoftTissues(i)) * (W(Adipose3,:) - W(AdrenalGland,:)) + W(AdrenalGland,:);
    elseif (HU_SoftTissues(i,1) <= HU_SoftTissues(ConnectiveTissue,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(SmallIntestine,1))
        Density_SoftTissues(i,1) = (rho(SmallIntestine)*HU_SoftTissues(ConnectiveTissue) - rho(ConnectiveTissue)*HU_SoftTissues(SmallIntestine) + (rho(ConnectiveTissue) - rho(SmallIntestine))*HU_SoftTissues(i))/(HU_SoftTissues(ConnectiveTissue) - HU_SoftTissues(SmallIntestine));
        W_SoftTissues(i,:) = rho(SmallIntestine)*(HU_SoftTissues(ConnectiveTissue) - HU_SoftTissues(i)) / (rho(SmallIntestine)*HU_SoftTissues(ConnectiveTissue) - rho(ConnectiveTissue)*HU_SoftTissues(SmallIntestine) + (rho(ConnectiveTissue)-rho(SmallIntestine))*HU_SoftTissues(i)) * (W(SmallIntestine,:) - W(ConnectiveTissue,:)) + W(ConnectiveTissue,:);
    elseif (HU_SoftTissues(i,1) < HU_SoftTissues(Adipose3,1) && HU_SoftTissues(i,1) >= HU_SoftTissues(end,1))
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(end,1) HU_SoftTissues(Adipose3,1)], [rho(end) rho(Adipose3)], HU_SoftTissues(i,1));
        W_SoftTissues(i,:) = W(i,:);
    elseif (HU_SoftTissues(i,1) < (HU_SoftTissues(AdrenalGland)+HU_SoftTissues(SmallIntestine))/2)
        W_SoftTissues(i,:) = W(AdrenalGland,:);
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(AdrenalGland,1) HU_SoftTissues(SmallIntestine,1)], [rho(AdrenalGland) rho(SmallIntestine)], HU_SoftTissues(i,1));  
    elseif (HU_SoftTissues(i,1) >= (HU_SoftTissues(AdrenalGland)+HU_SoftTissues(SmallIntestine))/2)
        W_SoftTissues(i,:) = W(SmallIntestine,:);
        Density_SoftTissues(i,1) = interp1([HU_SoftTissues(AdrenalGland,1) HU_SoftTissues(SmallIntestine,1)], [rho(AdrenalGland) rho(SmallIntestine)], HU_SoftTissues(i,1));
    end
end

% Schneider materials
HU_Schneider = zeros(24,1);
HU_Schneider(1) = HU_SoftTissues(end);
HU_Schneider(2) = HU_SoftTissues(Lung);
HU_Schneider(3) = HU_SoftTissues(Adipose3);
k = 1:4;
rho1 = rho(Adipose3);
rho2 = rho(AdrenalGland);
w1 = W(Adipose3,k);
w2 = W(AdrenalGland,k);
H1 = HU_SoftTissues(Adipose3);
H2 = HU_SoftTissues(AdrenalGland);
for i=4:6
    HU_Sch = ((rho1*H2-rho2*H1)*(Compo(i,k)-w2)./(w1 - w2) - rho1*H2) ./ (-rho1 - ((rho2-rho1)*(Compo(i,k)-w2)./(w1 - w2)));
    HU_Schneider(i) = mean(HU_Sch);
end
HU_Schneider(7) = HU_SoftTissues(AdrenalGland);
HU_Schneider(8) = mean(HU_SoftTissues(HU_SoftTissues < HU_SoftTissues(ConnectiveTissue) & HU_SoftTissues >= HU_SoftTissues(SmallIntestine)));
HU_Schneider(9) = HU_SoftTissues(ConnectiveTissue);


%% Calibration of Schneider 2000 for bones, using previously found K

% Data
Z = [1 6 7 8 11 12 15 16 17 19 20 26];
A = [1.008 12.011 14.007 15.999 22.99 24.305 30.974 32.060 35.453 39.098 40.078 55.845];
w_w = [0.1119 0 0 0.8881 0 0 0 0 0 0 0 0];

% Read info on tissues
W = load(fullfile(DataDir, 'CompoWW_SkeletalTissues.txt'))/100;
rho = load(fullfile(DataDir, 'MassDensityWW_SkeletalTissues.txt'))/1000;
Names_B = textread(fullfile(DataDir, 'NamesWW_SkeletalTissues.txt'), '%s', 'delimiter', '\n');

% Indices of needed tissues
CorticalBone = 2;
RMarrow = 3;
YMarrow = 5;

% Add a tissue which is mixture of red and yellow marrows
W(end+1,:) = 0.5*W(YMarrow,:) + 0.5*W(RMarrow,:);
rho(end+1) = 1; % from Schneider 2000
W_B_exact = W;
Names_B{end+1} = 'mixed marrow';

% HU of materials needed for interpolationHU_Schneider(1) = HU_SoftTissues(end);
HU_Schneider(2) = HU_SoftTissues(Lung);
HU_Schneider(3) = HU_SoftTissues(Adipose3);
mu_w = rho_w*avo*sum(w_w./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)));
HU_SkeletalTissues(CorticalBone,1) = 1000*(rho(CorticalBone)*avo*sum(W(CorticalBone,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);
HU_SkeletalTissues(length(rho),1) = 1000*(rho(end)*avo*sum(W(end,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)))/mu_w - 1);

% Calibration
for i=1:size(W,1)
    mu_reftissues(i,1) = rho(i)*avo*sum(W(i,:)./A.*(Z + sol(1)*Z.^2.86 + Z.^4.62*sol(2)));
    HU_SkeletalTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1); % HU_SkeletalTissues(i,1) = 1000*(mu_reftissues(i)/mu_w-1) !!!!!!!!!!!!!!!!!!!!!!!!
    Density_SkeletalTissues(i,1) = (rho(end)*HU_SkeletalTissues(CorticalBone) - rho(CorticalBone)*HU_SkeletalTissues(end) + (rho(CorticalBone) - rho(end))*HU_SkeletalTissues(i))/(HU_SkeletalTissues(CorticalBone) - HU_SkeletalTissues(end));
    W_SkeletalTissues(i,:) = rho(end)*(HU_SkeletalTissues(CorticalBone) - HU_SkeletalTissues(i)) / (rho(end)*HU_SkeletalTissues(CorticalBone) - rho(CorticalBone)*HU_SkeletalTissues(end) + (rho(CorticalBone)-rho(end))*HU_SkeletalTissues(i)) * (W(end,:) - W(CorticalBone,:)) + W(CorticalBone,:);
    W_check(i,:) = (1524-HU_SkeletalTissues(i,1))/(1566+0.92*HU_SkeletalTissues(i,1))*(W(end,:) - W(CorticalBone,:)) + W(CorticalBone,:);
end

% Schneider materials
k = [1:4 7 11];
rho1 = rho(end);
rho2 = rho(CorticalBone);
w1 = W(end,k);
w2 = W(CorticalBone,k);
H1 = HU_SkeletalTissues(end);
H2 = HU_SkeletalTissues(CorticalBone);
for i=10:23
    HU_Sch = ((rho1*H2-rho2*H1)*(Compo(i,k)-w2)./(w1 - w2) - rho1*H2) ./ (-rho1 - ((rho2-rho1)*(Compo(i,k)-w2)./(w1 - w2)));
    HU_Schneider(i) = mean(HU_Sch);
end
HU_Schneider(24) = HU_SkeletalTissues(CorticalBone);


%% Density calibration curve

clearvars HU;
HU = [HU_SoftTissues(end)-100; HU_SoftTissues(end); HU_SoftTissues(Adipose3); HU_SoftTissues(AdrenalGland); HU_SoftTissues(SmallIntestine); HU_SoftTissues(ConnectiveTissue); HU_SoftTissues(ConnectiveTissue)+1; HU_SkeletalTissues(CorticalBone)];
HU = round(HU);
D = [Density_SoftTissues(end); Density_SoftTissues(end); Density_SoftTissues(Adipose3); Density_SoftTissues(AdrenalGland); Density_SoftTissues(SmallIntestine); Density_SoftTissues(ConnectiveTissue); interp1([HU_SkeletalTissues(end) HU_SkeletalTissues(CorticalBone)], [Density_SkeletalTissues(end) Density_SkeletalTissues(CorticalBone)], HU_SoftTissues(ConnectiveTissue)+1); Density_SkeletalTissues(CorticalBone)];
HU = [HU; HU(end)+10];
D = [D; D(end)]; % force constant extrapolation

% Write CC
mkdir(CT_CC_name);
fid = fopen(fullfile(CT_CC_name, 'HU_Density_Conversion.txt'),'w');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%s\n', '# HU	density g/cm3');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%d\t%1.6f\n', [HU D]');
fclose(fid);

% Graphical check
if (use_gui>0)
    figure
    plot([HU_SoftTissues; HU_SkeletalTissues], [Density_SoftTissues; Density_SkeletalTissues], '*')
    hold on
    plot(HU,D,'.-')
    xlabel('HU');
    ylabel('Mass density');
    legend('Tissues from ICRU database (interpolated)', 'Prediction of CT calibration')
    title('HU->Density curve');
end


%% Material calibration curve

% Delete skeletal tissues having HU smaller than connective tissue
ind = find(HU_SkeletalTissues <= HU_SoftTissues(ConnectiveTissue)); % remove bone/marrow tissues with HU below Connective tissue
HU_SkeletalTissues(ind) = [];
W_SkeletalTissues(ind,:) = [];
Density_SkeletalTissues(ind) = [];
W_B_exact(ind,:) = [];
Names_B(ind) = [];

% Group all calculated HU of ICRU materials
HU_All = [HU_SoftTissues; HU_SkeletalTissues];
W_All = [W_SoftTissues; W_SkeletalTissues];
Density_All = [Density_SoftTissues; Density_SkeletalTissues];
W_All_exact = [W_ST_exact; W_B_exact];
Names_All = [Names_ST; Names_B];
[HU_All,ind] = sort(HU_All);
W_All = W_All(ind,:);
Density_All = Density_All(ind);
W_All_exact = W_All_exact(ind,:);
Names_All = Names_All(ind);

% HU bins + correction for some specific tissues
HU_Bins = round([-1000; (HU_Schneider(2:end)+HU_Schneider(1:end-1))/2]);
HU_Bins(2) = -950;
HU_Bins(3) = round(HU_Schneider(3) - 25);
HU_Bins(8) = round(mean([HU_SoftTissues(AdrenalGland) HU_SoftTissues(SmallIntestine)]));
HU_Bins(10) = round(min([(HU_SoftTissues(ConnectiveTissue) + 25), HU_Schneider(10)]));
HU_Bins(11) = round(mean([HU_Schneider(10) HU_Schneider(11)]));

% Write HU_Material_Conversion
curve = [HU_Bins(:) mat_nb(:)];
curve = sortrows(curve,1);
fid = fopen(fullfile(CT_CC_name, 'HU_Material_Conversion.txt'),'w');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%s\n', '# HU material label');
fprintf(fid, '%s\n', '# ===================');
fprintf(fid, '%d\t%d\n', curve');
fclose(fid);

% Graphical check - for main elements
v = floor(HU_All(1)):1:floor(HU_All(end));
elements = {'H','C','N','O','Na','Mg','P','S','Cl','K','Ca','Fe'};
for i=1:size(W_All,2)
    CC(:,i) = 100*interp1(HU_Bins, Compo(:,i), v, 'previous', 'extrap');
end
for i=[1 2 3 4 7 11] % for hydrogen, carbon, nitrogen, oxygen, phosphorus and calcium
    if (use_gui>1)
        figure
        plot(HU_All, 100*W_All(:,i),'*-')
        hold on
        plot(v, CC(:,i),'.','MarkerSize', 8)
        hold on
        plot(HU_Schneider, 100*Compo(:,i), 'o', 'MarkerSize', 10.5)
        xlabel('HU');
        ylabel('Elemental fraction (%)')
        title(elements{i})
        legend('Tissues from ICRU database (interpolated)', 'Prediction of CT calibration', 'Schneider tissues');
    end
    err(:,i) = round((interp1(v, CC(:,i), HU_All, 'previous', 'extrap') - 100*W_All_exact(:,i)), 4);
    err_continuous(:,i) = round((100*W_All(:,i) - 100*W_All_exact(:,i)), 4);
    %err(:,i) = (interp1(v, CC(:,i), HU_SoftTissues, 'previous', 'extrap') - 100*W_SoftTissues(:,i));
    %err(:,i) = (interp1(v, CC(:,i), HU_SkeletalTissues, 'previous', 'extrap') - 100*W_SkeletalTissues(:,i));
    if (use_gui>1)
        figure
        hist(err(:,i), 15)
        xlabel('Error');
        title(elements{i})
    end
end


% Write errors next to corresponding tissues names
format short
Title{1,1} = '#######################################################';
Title{2,1} = '# Errors on elemental composition for each tissue (%) #';
Title{3,1} = '#######################################################';
Results = ['TISSUE' elements([1 2 3 4 7 11]); Names_All num2cell(err(:,[1 2 3 4 7 11]))];
fid = fopen('Results_CTcali.txt','w');
fprintf(fid, '%s\n', string(Title));
for i=1:size(Results,1)
    fprintf(fid, '%60s\t\t%s\t%s\t%s\t%s\t%s\t%s\n', string(Results(i,:)));
    fprintf(fid, '\n');
end
fclose(fid);
Title
Results
format long 




