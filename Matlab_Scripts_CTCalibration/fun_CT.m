function [ y ] = fun_CT(x, Z, A, w, w_w, rho_w, HU_th, rho)

% Calculate attenuation coefficients
mu_w = sum(w_w./A.*Z.*(1 + Z.^1.86*x(1) + Z.^3.62*x(2)));
mu1 = (Z./A.*(1 + Z.^1.86*x(1) + Z.^3.62*x(2)))*w';
mu_tot = rho(:)/rho_w.*mu1(:)/mu_w; % mu relative to water

% Objective function - least squares
y = sum((mu_tot - (HU_th(:)/1000 + 1)).^2);


end

