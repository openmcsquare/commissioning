clear all
close all
clc

thisPath = strsplit(mfilename('fullpath'), filesep);
toolPath = strjoin(thisPath(1:end-2), filesep);
addpath(genpath(toolPath));
    
runtests('test_import')