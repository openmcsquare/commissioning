
% test Raystation format
folderPath = '../Data/Commissioning_measurements/RayStation_sample';
[mdata,all_energies,isoc_depth,IC_diameter,mask_begin,mask_end,nozzle_dist] = read_PristineBP(fullfile(folderPath, 'Measured_PristineBraggPeaks.csv'));
assert(isequal(all_energies, [100 105 110]), 'all_energies not parsed properly in read_PristineBP for RayStation format')
assert(isequal(isoc_depth, [0 0 0]), 'isoc_depth not parsed properly in read_PristineBP for RayStation format')
assert(isequal(IC_diameter, [81.6 81.6 81.6]), 'IC_diameter not parsed properly in read_PristineBP for RayStation format')
assert(isequal(mask_begin, [20 91 166]'), 'mask_begin not parsed properly in read_PristineBP for RayStation format')
assert(isequal(mask_end, [78 153 234]'), 'mask_end not parsed properly in read_PristineBP for RayStation format')
assert(isequal(nozzle_dist, [430 430 430]), 'nozzle_dist not parsed properly in read_PristineBP for RayStation format')

[mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,nozzle_dist] = read_SpotProfiles(fullfile(folderPath, 'Measured_SpotProfiles.csv'));
assert(isequal(all_energies(1:10), 100*ones(1,10)), 'all_energies not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(all_energies(11:20), 105*ones(1,10)), 'all_energies not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(all_energies(21:30), 110*ones(1,10)), 'all_energies not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(all_depths(1:2:30), repmat([0 100 200 -100 -200], [1 3])), 'all_depths not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(all_depths(2:2:30), repmat([0 100 200 -100 -200], [1 3])), 'all_depths not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(all_curves, repmat(["X" "Y"], [1 15])), 'all_curves not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(mask_begin(1:5), [18 120 232 327 435]'), 'mask_begin not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(mask_begin(end-4:end), [2674 2784 2893 3004 3105]'), 'mask_begin not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(mask_end(1:5), [109 221 316 424 525]'), 'mask_end not parsed properly in read_SpotProfiles for RayStation format')
assert(isequal(mask_end(end-4:end), [2773 2882 2993 3094 3204]'), 'mask_end not parsed properly in read_SpotProfiles for RayStation format')

[Dose_meas,energy,isoc_depth,meas_depths,nbspots1,nbspots2,spot_spacing] = read_AbsoluteDosimetry(fullfile(folderPath, 'Measured_AbsoluteDosimetry.csv'));
assert(isequal(Dose_meas, [0.129433075550268 0.127870315288519 0.12576740035693]' / 100), 'Dose_meas not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(energy, [100 105 110]'), 'energy not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(isoc_depth, [30. 30. 30.]'), 'isoc_depth not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(meas_depths, [30. 30. 30.]'), 'meas_depths not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(nbspots1, 41), 'nbspots1 not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(nbspots2, 41), 'nbspots2 not parsed properly in read_AbsoluteDosimetry for RayStation format')
assert(isequal(spot_spacing, 2.5), 'spot_spacing not parsed properly in read_AbsoluteDosimetry for RayStation format')


% test Eclipse format with old dataset
folderPath = '../Data/Commissioning_measurements/Eclipse_old_sample';
[mdata,all_energies,isoc_depth,IC_diameter,mask_begin,mask_end,nozzle_dist] = read_PristineBP(fullfile(folderPath, 'IDD_sample.asc'));
assert(isequal(all_energies, [71.3 73.2 75.0]), 'all_energies not parsed properly in read_PristineBP for old Eclipse format')
assert(isnan(isoc_depth), 'isoc_depth not parsed properly in read_PristineBP for old Eclipse format')
assert(isnan(IC_diameter), 'IC_diameter not parsed properly in read_PristineBP for old Eclipse format')
assert(isequal(mask_begin, [9 4017 8025]'), 'mask_begin not parsed properly in read_PristineBP for old Eclipse format')
assert(isequal(mask_end, [4008 8016 12024]'), 'mask_end not parsed properly in read_PristineBP for old Eclipse format')
assert(isnan(nozzle_dist), 'nozzle_dist not parsed properly in read_PristineBP for old Eclipse format')

[mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,nozzle_dist] = read_SpotProfiles(fullfile(folderPath, 'spot_profiles.asc'));
assert(isequal(all_energies(1:5), 71.3*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_energies(6:10), 73.2*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_energies(11:15), 75.0*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_energies(16:20), 71.3*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_energies(21:25), 73.2*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_energies(26:30), 75.0*ones(1,5)), 'all_energies not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(all_depths, repmat([-200 -100 0 100 200], [1 6])), 'all_depths not parsed properly in read_SpotProfiles for  old Eclipse format')
assert(isequal(all_curves(1:15), repmat(["MeasuredSpotFluenceX"], [1 15])), 'all_curves not parsed properly in read_SpotProfiles for  old Eclipse format')
assert(isequal(all_curves(16:30), repmat(["MeasuredSpotFluenceY"], [1 15])), 'all_curves not parsed properly in read_SpotProfiles for  old Eclipse format')
assert(isequal(mask_begin(1:5), [10 1020 2030 3040 4050]'), 'mask_begin not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(mask_begin(end-4:end), [25260 26270 27280 28290 29300]'), 'mask_begin not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(mask_end(1:5), [1010 2020 3030 4040 5050]'), 'mask_end not parsed properly in read_SpotProfiles for old Eclipse format')
assert(isequal(mask_end(end-4:end), [26260 27270 28280 29290 30300]'), 'mask_end not parsed properly in read_SpotProfiles for old Eclipse format')

[Dose_meas,energy,isoc_depth,meas_depths,nbspots1,nbspots2,spot_spacing] = read_AbsoluteDosimetry(fullfile(folderPath, 'IDD_sample.asc'), 20., 0., [0. 0.], [1. 1.], 1., 1., 2e3, 2e3);
assert(isequal(Dose_meas, [83.92843838839329 82.29918635427899 81.05087736496422]'), 'Dose_meas not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(energy, [71.3 73.2 75.]), 'energy not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(isoc_depth, [0. 0. 0.]'), 'isoc_depth not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(meas_depths, [20. 20. 20.]'), 'meas_depths not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(nbspots1, 1), 'nbspots1 not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(nbspots2, 1), 'nbspots2 not parsed properly in read_AbsoluteDosimetry for old Eclipse format')
assert(isequal(spot_spacing, [1. 1.]), 'spot_spacing not parsed properly in read_AbsoluteDosimetry for old Eclipse format')

% test Eclipse format with new dataset
folderPath = '../Data/Commissioning_measurements/Eclipse_new_sample';
[mdata,all_energies,isoc_depth,IC_diameter,mask_begin,mask_end,nozzle_dist] = read_PristineBP(fullfile(folderPath, 'Measured_Depth_Dose_Parallel_Plate.txt'));
assert(isequal(all_energies, [70. 80. 90.]), 'all_energies not parsed properly in read_PristineBP for new Eclipse format')
assert(isnan(isoc_depth), 'isoc_depth not parsed properly in read_PristineBP for new Eclipse format')
assert(isnan(IC_diameter), 'IC_diameter not parsed properly in read_PristineBP for new Eclipse format')
assert(isequal(mask_begin, [16 103 198]'), 'mask_begin not parsed properly in read_PristineBP for new Eclipse format')
assert(isequal(mask_end, [87 182 333]'), 'mask_end not parsed properly in read_PristineBP for new Eclipse format')

SpotFiles = {fullfile(folderPath, 'Measured_Spot_Fluence_Profile_X.txt'), fullfile(folderPath, 'Measured_Spot_Fluence_Profile_Y.txt')};
[mdata,all_energies,all_depths,all_curves,mask_begin,mask_end,nozzle_dist] = read_SpotProfiles(SpotFiles);
assert(isequal(all_energies, repmat([70 80 90], [1 10])), 'all_energies not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(1:3:15), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(2:3:15), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(3:3:15), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(16:3:30), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(17:3:30), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_depths(18:3:30), [150 300 -150 -300 0]), 'all_depths not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_curves(1:15), repmat(["MeasuredSpotFluenceX"], [1 15])), 'all_curves not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(all_curves(16:30), repmat(["MeasuredSpotFluenceY"], [1 15])), 'all_curves not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(mask_begin(1:5), [18 302 569 822 1072]'), 'mask_begin not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(mask_begin(end-4:end), [7553 7933 8285 8607 8906]'), 'mask_begin not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(mask_end(1:5), [284 551 804 1054 1290]'), 'mask_end not parsed properly in read_SpotProfiles for new Eclipse format')
assert(isequal(mask_end(end-4:end), [7915 8267 8589 8888 9167]'), 'mask_end not parsed properly in read_SpotProfiles for new Eclipse format')

[Dose_meas,energy,isoc_depth,meas_depths,nbspots1,nbspots2,spot_spacing] = read_AbsoluteDosimetry(fullfile(folderPath, 'Measured_Depth_Dose_Parallel_Plate.txt'), 20., 0., [0. 0.], [1. 1.], 1., 1., 2e3, 2e3);
assert(isequal(Dose_meas, [0.45333790804823056 0.4203509459856876 0.4021174394667189]'), 'Dose_meas not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(energy, [70 80 90]), 'energy not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(isoc_depth, [0. 0. 0.]'), 'isoc_depth not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(meas_depths, [20. 20. 20.]'), 'meas_depths not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(nbspots1, 1), 'nbspots1 not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(nbspots2, 1), 'nbspots2 not parsed properly in read_AbsoluteDosimetry for new Eclipse format')
assert(isequal(spot_spacing, [1. 1.]), 'spot_spacing not parsed properly in read_AbsoluteDosimetry for new Eclipse format')