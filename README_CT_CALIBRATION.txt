Prerequisites:
----------------
- A recent version of Matlab is required, including the optimization toolbox
- Measured HU in a text file, plus mass density and elemental composition of scanned phantom inserts (format: see sample data for Gammex or CIRS phantom)


Getting started:
-----------------
The code implements the stoichiometric calibration of Schneider 2000 (for Monte Carlo algorithms). 
A sample set of HU is given for the Gammex phantom (Data -> CT_calibration -> Gammex_phantom), along with info on the phantom. This format should be used for your measurement. In the phantom comopsition file, the two first lines are respectively the atomic numbers and atomic masses of elements present in the phantom. They can be changed. Running the script as it is will produce a calibration curve for these HU, in the Scanners folder of MCsquare directory given there. The code will also print some info on the results and some figures allowing to assess the calibration curve obtained.


Generate a BDL for your facility:
---------------------------------
- The code to be launched is "main.m". 
- The user must only modify the first part of the file "main.m", named "Input data - To be modified by user".
- The folder "DataDir" mentioned "in main.m" is the folder containing all info on phantom (HU, composition, names of inserts, mass densities).
- Choose a name for your calibration curve.
- Run the code (it should take a few seconds).
- The calibration curve is created where you chose. It's composed of a file "HU_Density_Conversion.txt" and a file "HU_Material_Conversion.txt". Both files are generated totally independently. You can use them as it is or choose to keep your usual HU->density calibration curve and just add the HU->material conversion.
- A file "Results_CTcali.txt" is also produced by the code, in the Matlab_Scripts_CTCalibration folder. It gives an idea of the error made on the elemental composition of some usual ICRU tissues when using the created calibration curve. 
